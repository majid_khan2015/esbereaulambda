
package com.equifax.services.eport.ws.schemas._1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DimensionalVariableType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DimensionalVariableType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TDA_MESMI_CC_PSDAMT_24" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TDA_MESME_INS_PSDAMT_24" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TDA_METSU_CC_PSDAMT_3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TDA_SUM_PF_PSDAMT_3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DimensionalVariableType", propOrder = {
    "tdamesmiccpsdamt24",
    "tdamesmeinspsdamt24",
    "tdametsuccpsdamt3",
    "tdasumpfpsdamt3"
})
public class DimensionalVariableType {

    @XmlElement(name = "TDA_MESMI_CC_PSDAMT_24")
    protected BigDecimal tdamesmiccpsdamt24;
    @XmlElement(name = "TDA_MESME_INS_PSDAMT_24")
    protected BigDecimal tdamesmeinspsdamt24;
    @XmlElement(name = "TDA_METSU_CC_PSDAMT_3")
    protected BigDecimal tdametsuccpsdamt3;
    @XmlElement(name = "TDA_SUM_PF_PSDAMT_3")
    protected BigDecimal tdasumpfpsdamt3;

    /**
     * Gets the value of the tdamesmiccpsdamt24 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTDAMESMICCPSDAMT24() {
        return tdamesmiccpsdamt24;
    }

    /**
     * Sets the value of the tdamesmiccpsdamt24 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTDAMESMICCPSDAMT24(BigDecimal value) {
        this.tdamesmiccpsdamt24 = value;
    }

    /**
     * Gets the value of the tdamesmeinspsdamt24 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTDAMESMEINSPSDAMT24() {
        return tdamesmeinspsdamt24;
    }

    /**
     * Sets the value of the tdamesmeinspsdamt24 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTDAMESMEINSPSDAMT24(BigDecimal value) {
        this.tdamesmeinspsdamt24 = value;
    }

    /**
     * Gets the value of the tdametsuccpsdamt3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTDAMETSUCCPSDAMT3() {
        return tdametsuccpsdamt3;
    }

    /**
     * Sets the value of the tdametsuccpsdamt3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTDAMETSUCCPSDAMT3(BigDecimal value) {
        this.tdametsuccpsdamt3 = value;
    }

    /**
     * Gets the value of the tdasumpfpsdamt3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTDASUMPFPSDAMT3() {
        return tdasumpfpsdamt3;
    }

    /**
     * Sets the value of the tdasumpfpsdamt3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTDASUMPFPSDAMT3(BigDecimal value) {
        this.tdasumpfpsdamt3 = value;
    }

}
