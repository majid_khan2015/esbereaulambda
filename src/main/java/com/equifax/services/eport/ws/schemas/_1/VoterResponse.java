
package com.equifax.services.eport.ws.schemas._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for voterResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="voterResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://services.equifax.com/eport/ws/schemas/1.0}response">
 *       &lt;sequence>
 *         &lt;element name="voterRespId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Voter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="percentMatch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="middleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastUpdatedDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="guardianName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="guardianType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="age" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="section" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="city_village_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="postoffice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="panchayat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tehsil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="district" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pincode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pincode2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="voterResponse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "voterResponse", propOrder = {
    "voterRespId",
    "voter",
    "percentMatch",
    "title",
    "firstName",
    "middleName",
    "lastName",
    "lastUpdatedDate",
    "guardianName",
    "guardianType",
    "gender",
    "age",
    "address",
    "hno",
    "section",
    "cityVillageName",
    "postoffice",
    "panchayat",
    "tehsil",
    "district",
    "pincode",
    "pincode2",
    "state",
    "voterResponse"
})
public class VoterResponse
    extends Response
{

    protected String voterRespId;
    @XmlElement(name = "Voter")
    protected String voter;
    protected String percentMatch;
    protected String title;
    protected String firstName;
    protected String middleName;
    protected String lastName;
    protected String lastUpdatedDate;
    protected String guardianName;
    protected String guardianType;
    protected String gender;
    protected String age;
    protected String address;
    protected String hno;
    protected String section;
    @XmlElement(name = "city_village_name")
    protected String cityVillageName;
    protected String postoffice;
    protected String panchayat;
    protected String tehsil;
    protected String district;
    protected String pincode;
    protected String pincode2;
    protected String state;
    protected String voterResponse;

    /**
     * Gets the value of the voterRespId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoterRespId() {
        return voterRespId;
    }

    /**
     * Sets the value of the voterRespId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoterRespId(String value) {
        this.voterRespId = value;
    }

    /**
     * Gets the value of the voter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoter() {
        return voter;
    }

    /**
     * Sets the value of the voter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoter(String value) {
        this.voter = value;
    }

    /**
     * Gets the value of the percentMatch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPercentMatch() {
        return percentMatch;
    }

    /**
     * Sets the value of the percentMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPercentMatch(String value) {
        this.percentMatch = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the lastUpdatedDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * Sets the value of the lastUpdatedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastUpdatedDate(String value) {
        this.lastUpdatedDate = value;
    }

    /**
     * Gets the value of the guardianName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuardianName() {
        return guardianName;
    }

    /**
     * Sets the value of the guardianName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuardianName(String value) {
        this.guardianName = value;
    }

    /**
     * Gets the value of the guardianType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuardianType() {
        return guardianType;
    }

    /**
     * Sets the value of the guardianType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuardianType(String value) {
        this.guardianType = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGender(String value) {
        this.gender = value;
    }

    /**
     * Gets the value of the age property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAge() {
        return age;
    }

    /**
     * Sets the value of the age property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAge(String value) {
        this.age = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * Gets the value of the hno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHno() {
        return hno;
    }

    /**
     * Sets the value of the hno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHno(String value) {
        this.hno = value;
    }

    /**
     * Gets the value of the section property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSection() {
        return section;
    }

    /**
     * Sets the value of the section property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSection(String value) {
        this.section = value;
    }

    /**
     * Gets the value of the cityVillageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCityVillageName() {
        return cityVillageName;
    }

    /**
     * Sets the value of the cityVillageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCityVillageName(String value) {
        this.cityVillageName = value;
    }

    /**
     * Gets the value of the postoffice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostoffice() {
        return postoffice;
    }

    /**
     * Sets the value of the postoffice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostoffice(String value) {
        this.postoffice = value;
    }

    /**
     * Gets the value of the panchayat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPanchayat() {
        return panchayat;
    }

    /**
     * Sets the value of the panchayat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPanchayat(String value) {
        this.panchayat = value;
    }

    /**
     * Gets the value of the tehsil property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTehsil() {
        return tehsil;
    }

    /**
     * Sets the value of the tehsil property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTehsil(String value) {
        this.tehsil = value;
    }

    /**
     * Gets the value of the district property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistrict() {
        return district;
    }

    /**
     * Sets the value of the district property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistrict(String value) {
        this.district = value;
    }

    /**
     * Gets the value of the pincode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPincode() {
        return pincode;
    }

    /**
     * Sets the value of the pincode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPincode(String value) {
        this.pincode = value;
    }

    /**
     * Gets the value of the pincode2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPincode2() {
        return pincode2;
    }

    /**
     * Sets the value of the pincode2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPincode2(String value) {
        this.pincode2 = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the voterResponse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoterResponse() {
        return voterResponse;
    }

    /**
     * Sets the value of the voterResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoterResponse(String value) {
        this.voterResponse = value;
    }

}
