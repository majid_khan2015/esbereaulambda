
package com.equifax.services.eport.ws.schemas._1;

import lombok.Data;

@Data
public class RecentActivitiesType {

    private Integer AccountsDeliquent;
    private Integer AccountsOpened;
    private Integer TotalInquiries;
    private Integer AccountsUpdated;

   
}
