package com.es.scores.update;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaAsync;
import com.amazonaws.services.lambda.AWSLambdaAsyncClientBuilder;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;

public final class AccessLambda {
	
	private AccessLambda() {super();};
	private static AWSLambda client = null;
	private static AWSLambdaAsync asynClient = null;
	
	
	
	public static AWSLambda getAWSClient() {
		if(client!=null) {
			return client;
		}
		return  client = AWSLambdaClientBuilder.standard().withRegion(Regions.AP_SOUTH_1)
				//.withCredentials(new AWSStaticCredentialsProvider(creds))
				.build();
	}
	
	
	
	
	public static AWSLambdaAsync getAWSAysncClient() {
		if(asynClient!=null) {
			return asynClient;
		}
		return  asynClient = AWSLambdaAsyncClientBuilder.standard().withRegion(Regions.AP_SOUTH_1)
				//.withCredentials(new AWSStaticCredentialsProvider(creds))
				.build();
	}
	

	
}
