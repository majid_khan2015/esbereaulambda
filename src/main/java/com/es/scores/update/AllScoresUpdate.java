/**
 * 
 */
package com.es.scores.update;

import java.nio.ByteBuffer;
import java.util.concurrent.Future;

import org.json.JSONObject;

import com.amazonaws.services.lambda.AWSLambdaAsync;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.es.client.business.ExternalApiCall;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauVo;
import com.es.client.model.FoirResponseModel;
import com.es.client.score.ESScoreService;
import com.google.gson.Gson;


public class AllScoresUpdate {
	//private static String foirLambda = "https://socialworth.in/foircalculation";
	private static final String foirLambda = System.getenv("foirLambda");
	private static final String breLambda = System.getenv("breLambda");

	public BureauVo allScoresUpdate(BureauVo bureauVo, int subsId, double salary) {

		try {
			Long customer_id = new BureauDbDao().getCustomerCode(subsId);
			bureauVo = new BureauDbDao().getCirDetails(bureauVo, subsId);
			System.out.println("customer id :: " + customer_id);
			if (customer_id > 0) {
				// Request Creation For Foir Call
				bureauVo.setCustomer_id(customer_id);
				bureauVo.setSubs_id(subsId);
				bureauVo.setSalary(salary);
				JSONObject foirObject = foirRequest(bureauVo);
				FoirResponseModel response = new ExternalApiCall().FoirApi(foirObject, foirLambda);
				if (response.getStatus_code() == 200) {
					bureauVo.setFoir(response.getFoir_value());
				}
				System.out.println("foir response :: " + new Gson().toJson(response));
				// call es score
				bureauVo = new ESScoreService().getEsScore(bureauVo);
				//for production testing of new BRE API-2
				callBreLambda(subsId);
			} else { 
				System.out.println("customer not found for FOIR Calculation");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bureauVo;

	}
	
	
	private void callBreLambda(int subsId) {
		try {
			
			
			System.out.println("callOnBoardApi Call Started----->");
			AWSLambdaAsync asynClient = AccessLambda.getAWSAysncClient();
			
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("requestId", "breAPI2");
			jsonObject.put("source", "auto");
			jsonObject.put("subscriberId", subsId);
			
			InvokeRequest req = new InvokeRequest().withFunctionName(breLambda)
					.withPayload(ByteBuffer.wrap(jsonObject.toString().getBytes()))
					.withInvocationType("Event");
			
			
			Future<InvokeResult> future_res = asynClient.invokeAsync(req);
			InvokeResult res = future_res.get();
			System.out.println("Calling Bre 2 Api:"+res);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	

	private JSONObject foirRequest(BureauVo vo) {
		JSONObject foirObject = new JSONObject();
		foirObject.put("customer_id", vo.getCustomer_id());
		foirObject.put("salary", vo.getSalary());
		foirObject.put("subs_id", vo.getSubs_id());
		foirObject.put("request_type", 1);
		return foirObject;
	}

	/*public static void main(String[] args) {
		BureauVo vo = new BureauVo();
		new AllScoresUpdate().allScoresUpdate(vo, 33069, 30000);
	}*/
}
