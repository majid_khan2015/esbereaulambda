package com.es.equifax;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import com.equifax.services.eport.servicedefs._1.CreditReportWSInquiryPortType;
import com.equifax.services.eport.ws.schemas._1.DimensionalVariableType;
import com.equifax.services.eport.ws.schemas._1.EmailAddressType;
import com.equifax.services.eport.ws.schemas._1.InquiryRequestType;
import com.equifax.services.eport.ws.schemas._1.InquiryResponseType;
import com.equifax.services.eport.ws.schemas._1.RequestBodyType;
import com.equifax.services.eport.ws.schemas._1.RequestHeaderType;
import com.es.client.BureauResponseParse.AccountSummaryParser;
import com.es.client.BureauResponseParse.AddressParsing;
import com.es.client.BureauResponseParse.BureauAccountParsing;
import com.es.client.BureauResponseParse.BureauReportParser;
import com.es.client.BureauResponseParse.ContactParsing;
import com.es.client.BureauResponseParse.EnquirySummaryparser;
import com.es.client.BureauResponseParse.EnquiryinfoParser;
import com.es.client.BureauResponseParse.IdentityInfoParsing;
import com.es.client.BureauResponseParse.OtherKeyIdentifyParser;
import com.es.client.BureauResponseParse.PersonalInfoParsing;
import com.es.client.BureauResponseParse.RecentActivityParser;
import com.es.client.BureauResponseParse.ScoreElementParser;
import com.es.client.business.ConstructRequestBody;
import com.es.client.business.ConstructRequestHeader;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauEmailAddressInformationModel;
import com.es.client.model.BureauReportResponseData;
import com.es.client.model.BureauSubscriberDetailsModel;
import com.es.client.model.BureauVo;
import com.es.client.model.DimensionVariableModel;
import com.es.client.util.MailService;
import com.es.scores.update.AllScoresUpdate;
import com.google.gson.Gson;



public class EquifaxUtil {
	private int INTERNAL_SERVER_ERROR = 500;
	private Integer EXPECTATION_FAILED = 417;
	
	private final static Gson gson = new Gson();

	public BureauVo invokeEquifaxWSService(String URL, List<BureauSubscriberDetailsModel> responseSubscriberDetail,
			int bureauType, String reqType, String panId, int bureauRequestId, Double salary) {
		BureauVo vo = new BureauVo();
		BureauDbDao bureaudao = null;
		InquiryResponseType responseType = null;
		int bureauReportDataForeignKey = 0;
		int bureau_report_id = 0, subs_id = 0, pincode = 0, bureau_req_id = 0;
		String success = "success", dob = "";
		RequestBodyType requestBody = null;
		for (BureauSubscriberDetailsModel responseSubscriberDetails : responseSubscriberDetail) {
			subs_id = responseSubscriberDetails.getSubscriberId();
			pincode = responseSubscriberDetails.getPincode();
			dob = responseSubscriberDetails.getDateOfBirth();
		}
		System.out.println("pan id in Equifax Util : " + panId);
		if (bureauType == 1 && (reqType.equals("CIR")) && panId != null) {
			System.out.println("CIR Request Logger -- >");
			java.net.URL endpoint = null;

			// ******** TURN OFF SSL CERTIFICATE *******//
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };
			SSLContext sc = null;

			// Install the all-trusting trust manager
			try {
				sc = SSLContext.getInstance("SSL");

				sc.init(null, trustAllCerts, new java.security.SecureRandom());
			} catch (KeyManagementException e2) {
				e2.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			// ********END TURN OFF SSL CERTIFICATE *******//
			try {
				endpoint = new java.net.URL(URL);
			} catch (java.net.MalformedURLException e1) {
				e1.printStackTrace();
			}
			try {
				QName qname = new QName("http://services.equifax.com/eport/servicedefs/1.0", "v1.0");
				Service service = Service.create(endpoint, qname);
				CreditReportWSInquiryPortType inquiryPortType = service.getPort(CreditReportWSInquiryPortType.class);
				BindingProvider bp = (BindingProvider) inquiryPortType;
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, URL);
				((BindingProvider) bp).getRequestContext().put("javax.xml.ws.client.connectionTimeout", "6000");
				// Set timeout until the response is received
				((BindingProvider) bp).getRequestContext().put("javax.xml.ws.client.receiveTimeout", "1000");
				InquiryRequestType input = new InquiryRequestType();
				RequestHeaderType requestHeader = new RequestHeaderType();
				ConstructRequestBody constructRequestBody = new ConstructRequestBody();
				ConstructRequestHeader constructRequestHeader = new ConstructRequestHeader();

				requestHeader = constructRequestHeader.populateUATRetailRequestHeaderType();
				input.setRequestHeader(requestHeader);

				requestBody = constructRequestBody.constructRetailRequest(responseSubscriberDetail, panId);
				/**
				 * create request transaction log with pending status
				 */
				try {
					bureaudao = new BureauDbDao();
					bureau_req_id = bureaudao.insertRequestLog(requestBody, panId, pincode, dob, subs_id);
				} catch (Exception e) {
					System.out.println("error in bureau request---->");
					e.printStackTrace();
				}
				input.setRequestBody(requestBody);
				System.out.println("requestHeader for Equifax : "+gson.toJson(requestHeader));
				System.out.println("requestBody for Equifax	: "+gson.toJson(requestBody));
				//soap Call
				responseType = inquiryPortType.getConsumerCreditReport(input);
				System.out.println("subsciber id :: " + subs_id);
				System.out.println("Customer CIR response	: " + gson.toJson(responseType));
				System.out.println("------------------");
				System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
				System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
				System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
				System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

				
				//for error handling
				/*if(responseType.getReportData() == null) {
					//send mail to
					new MailService().sendEmail();
				}else {
					if(responseType.getReportData().getError()!=null
							&& responseType.getReportData().getError().isEmpty()) {
						//send mail to
						new MailService().sendEmail();
					}
				}*/
				
				
				
				// PROCESS BUREAU REPORT RESPONSE DATA INFORMATION
				if (responseType.getInquiryResponseHeader() != null) {
					BureauReportResponseData bureauReportResponseData = new BureauReportParser()
							.bureauReportParser(responseType);
					vo.setCir(bureauReportResponseData.getScoreValue());
					vo.setScoreElement(bureauReportResponseData.getScoringElementCode());

					// data insert in mysql bureau_report_response_data table
					bureau_report_id = new BureauDbDao().insertDataToBureauResponseData(bureauReportResponseData,
							subs_id);
					if (bureau_report_id == 0) {
						success = "failure";
						if (bureau_req_id > 0)
							updateBureauRecord(bureau_req_id, panId, success);
						vo.setStatus_code("201");
						vo.setStatus(INTERNAL_SERVER_ERROR);
						vo.setMessage("Failure to process customer");
						return vo;
					} else {
						bureauReportDataForeignKey = bureau_report_id;
					}

					String scoringEleCode = bureauReportResponseData.getScoringElementCode();
				//	vo.setScoreElement(scoringEleCode);
					System.out.println("scoring element is :: " + scoringEleCode);
					if (scoringEleCode.equalsIgnoreCase("NoHit")) {
						if (bureau_req_id > 0)
							updateBureauRecord(bureau_req_id, panId, success);
					} else {
						// PROCESS BUREAU PERSONAL INFORMATION
						if (responseType.getReportData().getIDAndContactInfo().getPersonalInfo() != null) {
							new PersonalInfoParsing().personalInfoParsing(
									responseType.getReportData().getIDAndContactInfo().getPersonalInfo(),
									bureauReportDataForeignKey);

						}
						// PROCESS BUREAU SCORE ELEMENTS
						if (!responseType.getReportData().getScoringElements().getScoringElement().isEmpty()) {
							int scoreTagsSize = responseType.getReportData().getScoringElements().getScoringElement()
									.size();
							if (scoreTagsSize > 0) {
								new ScoreElementParser().scoreElementParser(bureauReportDataForeignKey,
										responseType.getReportData().getScoringElements().getScoringElement());
							}
						}

						// PROCESS BUREAU IDENTITY INFORMATION
						if (responseType.getReportData().getIDAndContactInfo().getIdentityInfo() != null) {

							new IdentityInfoParsing().identityInfoParsing(bureauReportDataForeignKey,
									responseType.getReportData().getIDAndContactInfo().getIdentityInfo());

						}

						// PROCESS BUREAU ADDRESS INFORMATION
						int sizeOfAddress = 0;
						if (responseType.getReportData().getIDAndContactInfo().getAddressInfo() != null) {
							sizeOfAddress = responseType.getReportData().getIDAndContactInfo().getAddressInfo().size();
							if (sizeOfAddress > 0) {
								new AddressParsing().addressparsing(
										responseType.getReportData().getIDAndContactInfo().getAddressInfo(),
										bureauReportDataForeignKey);
							}
						}

						// PROCESS BUREAU CONTACT INFORMATION
						int sizeOfContactInfo = 0;
						if (responseType.getReportData().getIDAndContactInfo().getPhoneInfo() != null) {
							sizeOfContactInfo = responseType.getReportData().getIDAndContactInfo().getPhoneInfo()
									.size();
							if (sizeOfContactInfo > 0) {
								new ContactParsing().contactparsing(
										responseType.getReportData().getIDAndContactInfo().getPhoneInfo(),
										bureauReportDataForeignKey);
							}
						}

						/**
						 * PROCESS BUREAU EMAIL ADDRESS
						 */
						int sizeOfEmailAddress = 0;
						if (responseType.getReportData().getIDAndContactInfo().getEmailAddressInfo() != null) {
							sizeOfEmailAddress = responseType.getReportData().getIDAndContactInfo()
									.getEmailAddressInfo().size();
						}
						if (sizeOfEmailAddress > 0) {
							BureauEmailAddressInformationModel bureauEmailAddressInformation = new BureauEmailAddressInformationModel();

							for (EmailAddressType emailAddressType : responseType.getReportData().getIDAndContactInfo()
									.getEmailAddressInfo()) {
								bureauEmailAddressInformation
										.setEmailAddress(emailAddressType.getEmailAddress().toLowerCase());
								/**
								 * data insert in mysql
								 * bureau_email_address_info table
								 */
								try {
									bureaudao = new BureauDbDao();

									bureaudao.insertDataToBureauEmailAddressInfo(bureauEmailAddressInformation,
											bureauReportDataForeignKey);
								} catch (Exception e) {
									System.out.println("Error in insertDataToBureauEmailAddressInfo----->");
									e.printStackTrace();

								}
							}
						}

						// PROCESS BUREAU ACCOUNT SUMMARY
						if (responseType.getReportData().getAccountSummary() != null) {
							new AccountSummaryParser().accountSummaryParser(bureauReportDataForeignKey,
									responseType.getReportData().getAccountSummary());
						}

						// PROCESS BUREAU RECENT ACTIVITY INFORMATION
						if (responseType.getReportData().getRecentActivities() != null) {
							new RecentActivityParser().recentActivityParser(bureauReportDataForeignKey,
									responseType.getReportData().getRecentActivities());
						}

						// PROCESS BUREAU OTHER KEY IDENTIFICATION INFORMATION
						if (responseType.getReportData().getOtherKeyInd() != null) {
							new OtherKeyIdentifyParser().otherKeyIdentifyParser(bureauReportDataForeignKey,
									responseType.getReportData().getOtherKeyInd());

						}
						// PROCESS BUREAU ENQUIRY SUMMARY INFORMATION
						if (responseType.getReportData().getEnquirySummary() != null) {
							new EnquirySummaryparser().enquirySummaryparser(
									responseType.getReportData().getEnquirySummary(), bureauReportDataForeignKey);

						}

						// PROCESS BUREAU ENQURIES INFO INFORMATION
						int sizeOfEnquiries = 0;
						if (responseType.getReportData().getEnquiries() != null) {
							sizeOfEnquiries = responseType.getReportData().getEnquiries().size();
							if (sizeOfEnquiries > 0) {
								new EnquiryinfoParser().enquiryinfoParser(bureauReportDataForeignKey,
										responseType.getReportData().getEnquiries());
							}
						}

						// Decimal Variable process information
						if (responseType.getReportData().getDimensionalVariable() != null) {
							DimensionalVariableType dimension = new DimensionalVariableType();
							dimension = responseType.getReportData().getDimensionalVariable();
							DimensionVariableModel dimensionVariableModel = new DimensionVariableModel();
							dimensionVariableModel
									.setTDA_MESME_INS_PSDAMT_24(dimension.getTDAMESMEINSPSDAMT24().doubleValue());
							dimensionVariableModel
									.setTDA_MESMI_CC_PSDAMT_24(dimension.getTDAMESMICCPSDAMT24().doubleValue());
							dimensionVariableModel
									.setTDA_METSU_CC_PSDAMT_3(dimension.getTDAMETSUCCPSDAMT3().doubleValue());
							dimensionVariableModel.setTDA_SUM_PF_PSDAMT_3(dimension.getTDASUMPFPSDAMT3().doubleValue());
							dimensionVariableModel.setBureauId(bureauReportDataForeignKey);

							bureaudao.insertDimensionVariable(dimensionVariableModel);
						}

						// PROCESS BUREAU ACCOUNT DETAILS INFORMATION
						int sizeOfAccountDetails = responseType.getReportData().getAccountDetails().getAccount().size();
						if (sizeOfAccountDetails > 0) {
							new BureauAccountParsing().bureauAccountsParsing(
									responseType.getReportData().getAccountDetails(), bureauReportDataForeignKey);
						}
						if (bureau_req_id > 0) {
							updateBureauRecord(bureau_req_id, panId, success);
						}
					}
					if (bureauRequestId == 1) {
						System.out.println(("subscriberId for Foir SCore : " + subs_id));
						vo = new AllScoresUpdate().allScoresUpdate(vo, subs_id, salary);
					}
					vo.setStatus(200);
					vo.setMessage("Success Equifax Call");
					vo.setIs_success(true);
					vo.setStatus_code("200");
				} else {
					System.out.println("@@@ error occured @@@");
					success = "failure";
				}
			} catch (Exception e) {
				vo.setStatus(EXPECTATION_FAILED);
				vo.setMessage("Error found in bureau Response");
				vo.setStatus_code("201");
				System.out.println("responseType from Equifax	: " + gson.toJson(responseType));
				System.out.println("error in service creation or may b something else-->>");
				success = "failure";
				new MailService().sendEmail();
				if (bureau_req_id > 0)
					updateBureauRecord(bureau_req_id, panId, success);
				e.printStackTrace();
			}

		} else if (bureauType == 2) {
			System.out.println("This is Experian Bureau ");
		} else {
			System.out.println("Invalid Request");
		}
		return vo;

	}

	private void updateBureauRecord(Integer bureau_req_id, String panId, String success) {
		try {
			new BureauDbDao().updateRequestStatus(bureau_req_id, panId, success);
		} catch (Exception e) {
			System.out.println("error in update request log :: ");
			e.printStackTrace();
		}
	}
}