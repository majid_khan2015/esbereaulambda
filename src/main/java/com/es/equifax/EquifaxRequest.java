/**
 * 
 */
package com.es.equifax;


public class EquifaxRequest {
 
	private int equifaxCustId = 4925;
	private String equifaxMemberNumber = "019FP04012";
	private String equifaxPassword = "Getrpt247";
	private String securityCode = "AS2";
	private String equifaxUserId = "STS_EARLYS";
	private String productCode = "PCS";
	private String productVersion = "1.0";

	private String inquiryPurpose = "05";
	private String address;
	private String city;
	private String firstName;
	private String gender;
	private String lastName;
	private String maritalStatus;
	private String pincode;
	private String state;
	private String subscriberId;
	private String mobileNumber;
	private String middleName;

	public String getInquiryPurpose() {
		return inquiryPurpose;
	}

	public void setInquiryPurpose(String inquiryPurpose) {
		this.inquiryPurpose = inquiryPurpose;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	public int getEquifaxCustId() {
		return equifaxCustId;
	}

	public void setEquifaxCustId(int equifaxCustId) {
		this.equifaxCustId = equifaxCustId;
	}

	public String getEquifaxMemberNumber() {
		return equifaxMemberNumber;
	}

	public void setEquifaxMemberNumber(String equifaxMemberNumber) {
		this.equifaxMemberNumber = equifaxMemberNumber;
	}

	public String getEquifaxPassword() {
		return equifaxPassword;
	}

	public void setEquifaxPassword(String equifaxPassword) {
		this.equifaxPassword = equifaxPassword;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getEquifaxUserId() {
		return equifaxUserId;
	}

	public void setEquifaxUserId(String equifaxUserId) {
		this.equifaxUserId = equifaxUserId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

}
