package com.es.client.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;





public class MailService {
	
	
	/*private static final String sendMailApiKey ="3023eb98aeff4e9d23b74063841a2ad3";
	private static final String myurl = "http://api.falconide.com/falconapi/web.send.rest?";
	private static final String subjectString = "GoCash Settlement Mismatch!!!";
	private static final String to = "vivek.kumar@earlysalary.com";
	private static final String cc = "om.kourav@earlysalary.com";
	private static String content = "Mismatch Found in repayment settled in gocash account and settlement in mloanTransaction table!!!";*/
	
	
	
	private static final String sendMailApiKey = System.getenv("sendMailApiKey");
	private static final String mailurl = System.getenv("mailurl");
	private static final String subjectString = System.getenv("subjectString");
	private static final String to = System.getenv("to");
	private static final String cc = System.getenv("cc");
	private static String content = System.getenv("content");

	public boolean sendEmail() {
		try {

			String subject = null;

			subject = URLEncoder.encode(subjectString + " " + new SimpleDateFormat("dd-MM-yyyy").format(new Date()),
					"UTF-8");
			content = URLEncoder.encode(content, "UTF-8");

			String from = URLEncoder.encode("noreply@earlysalary.com", "UTF-8");
			String api_key = URLEncoder.encode(sendMailApiKey, "UTF-8");
			String user_name = URLEncoder.encode("EarlySalary", "UTF-8");

			String msg = "api_key=" + api_key + "&subject=" + subject + "&fromname=" + user_name + "&from=" + from
					+ "&content=" + content + "&recipients=" + to + "&recipients_cc=" + cc;


			System.setProperty("jsse.enableSNIExtension", "false");
			URL url = new URL(mailurl);
			HttpURLConnection connection;

			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(msg.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");

			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(msg);
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			// TODO for response
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println("Email Status--->"+response);
			in.close();
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;

		}

	}
	
	
	/*public static void main(String[] args) {
		new MailService().sendEmail();
	}*/
	
	
	
	

}
