/**
 * 
 */
package com.es.client.util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author majidkhan
 *
 */
public final class ConnectionUtil {

	//qa
	
	private static String userName = System.getenv("userName");
	private static String password = System.getenv("password");
	private static String url = System.getenv("url");
	private static String driver = "com.mysql.jdbc.Driver";	
	
	
	//prod
	
/*	private static String userName = "rouser";
	private static String password = "RU@Es#123";
	private static String url = "jdbc:mysql://es.c5cc4erpgekm.ap-south-1.rds.amazonaws.com:7011/rbmanager";
	private static String driver = "com.mysql.jdbc.Driver";
	
*/	
	/*private static String userName = System.getenv("userName");
	private static String password = System.getenv("password");
	private static String url = System.getenv("url");
	private final static String driver = "com.mysql.jdbc.Driver";*/
	

	/*
	 * private static final String userName = System.getenv("dbuserName"); private
	 * static final String password = System.getenv("dbpassword"); private static
	 * final String url = System.getenv("dburl"); private static final String driver
	 * = System.getenv("dbdriver");
	 */

	public static Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, userName, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
}
