/**
 * 
 */
package com.es.client.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.equifax.services.eport.ws.schemas._1.RequestBodyType;
import com.es.client.model.BankAccountDetailsModel;
import com.es.client.model.BureauAccountDetailsModel;
import com.es.client.model.BureauAccountSummaryModel;
import com.es.client.model.BureauAddressInformationModel;
import com.es.client.model.BureauContactInformationModel;
import com.es.client.model.BureauEmailAddressInformationModel;
import com.es.client.model.BureauEnquiresInfoModel;
import com.es.client.model.BureauEnquirySummaryModel;
import com.es.client.model.BureauHistory48MonthsModel;
import com.es.client.model.BureauIdentityInformationModel;
import com.es.client.model.BureauOtherKeyIdentificationModel;
import com.es.client.model.BureauPersonalInformationModel;
import com.es.client.model.BureauRecentActivityInfoModel;
import com.es.client.model.BureauReportResponseData;
import com.es.client.model.BureauReqLogModel;
import com.es.client.model.BureauScoringElementsModel;
import com.es.client.model.BureauSubscriberDetailsModel;
import com.es.client.model.BureauVo;
import com.es.client.model.DimensionVariableModel;
import com.es.client.util.ConnectionUtil; 


public class BureauDbDao {


	/**
	 * AFTER BUREAU HIT INSERT DATE IN BUREAU REPORT RESPONSE DATA
	 * 
	 * @param BureauData
	 * @param subs_id
	 * @return
	 */
	public Integer insertDataToBureauResponseData(BureauReportResponseData BureauData, int subs_id) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";
		int count = 0;

		try {
			query = "insert into bureau_report_response_data(subs_id,report_order_no,product_code,success_code,date,time,hit_code,"
					+ "customer_code,client_id,inquiry_purpose,address_line,state,postal_code,phone_no,phone_type,pan_id,"
					+ "score_name,score_value,customer_ref_field,scoring_element_type,scoring_element_seq,scoring_element_code,"
					+ "scoring_element_desc,score_reason_code,score_desc,bureau_master_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			conn = ConnectionUtil.getConnection();

			pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, subs_id);
			pstmt.setString(2, BureauData.getReportOrderNo());
			pstmt.setString(3, BureauData.getProductCode());
			pstmt.setString(4, BureauData.getSuccessCode());
			pstmt.setString(5, BureauData.getDate());
			pstmt.setString(6, BureauData.getTime());
			pstmt.setString(7, BureauData.getHitCode());
			pstmt.setString(8, BureauData.getCustomerCode());
			pstmt.setString(9, BureauData.getClientId());
			pstmt.setString(10, BureauData.getInquiryPurpose());
			pstmt.setString(11, BureauData.getAddressLine());
			pstmt.setString(12, BureauData.getState());
			pstmt.setString(13, BureauData.getPostalCode());
			pstmt.setString(14, BureauData.getPhoneNo());
			pstmt.setString(15, BureauData.getPhoneType());
			pstmt.setString(16, BureauData.getPanId());
			pstmt.setString(17, BureauData.getScoreName());
			pstmt.setString(18, BureauData.getScoreValue());
			pstmt.setString(19, BureauData.getCustomerRefField());
			pstmt.setString(20, BureauData.getScoringElementType());
			pstmt.setInt(21, BureauData.getScoringElementSeq());
			pstmt.setString(22, BureauData.getScoringElementCode());
			pstmt.setString(23, BureauData.getScoringElementDesc());
			pstmt.setString(24, BureauData.getScoreReasonCode());
			pstmt.setString(25, BureauData.getScoreDesc());
			pstmt.setInt(26, 1);
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				count = rs.getInt(1);
				System.out.println("Auto Incremented Bureau Id-->> " + count + " and subscriber id is-->> " + subs_id);
			}
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data in master table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}

		return count;
	}

	/**
	 * INSERT BUREAU PERSONAL INFORMATION
	 * 
	 * @param personalData
	 * @param bureauAutoID
	 */
	public void insertDataToBureauPersonalInfo(BureauPersonalInformationModel personalData, int bureauAutoID) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";

		try {

			query = "insert into bureau_personal_info(bureau_report_data_id,first_name,middle_name,last_name,date_of_birth,"
					+ "gender,age,income,occupation_type) VALUES(?,?,?,?,?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			pstmt = conn.prepareStatement(query);

			pstmt.setInt(1, bureauAutoID);
			pstmt.setString(2, personalData.getFirstName());
			pstmt.setString(3, personalData.getMiddleName());
			pstmt.setString(4, personalData.getLastName());
			pstmt.setString(5, personalData.getDateOfBirth());
			pstmt.setString(6, personalData.getGender());
			pstmt.setString(7, personalData.getAge());
			pstmt.setString(8, personalData.getIncome());
			pstmt.setString(9, personalData.getOccupationType());
			pstmt.executeUpdate();

		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data personal info in table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}
	}

	/**
	 * INSERT BUREAU IDENTITY INFORMATION
	 * 
	 * @param identityInfoList
	 * @param bureauAutoID
	 */
	public void insertDataToBureauIdentityInfo(List<BureauIdentityInformationModel> identityInfoList,
			Integer bureauAutoID) {
		boolean success = true;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";
		try {

			query = "insert into bureau_identity_info(bureau_report_data_id,unique_id_no,unique_id_seq,"
					+ "unique_id_reported_date,id_type) VALUES(?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(query);
			for (int i = 0; i < identityInfoList.size(); i++) {
				BureauIdentityInformationModel identityInfo = (BureauIdentityInformationModel) identityInfoList.get(i);

				pstmt.setInt(1, bureauAutoID);
				pstmt.setString(2, identityInfo.getUniqueIdNo());
				pstmt.setInt(3, identityInfo.getUniqueIdSeq());
				pstmt.setString(4, identityInfo.getUniqueIdReportedDate());
				pstmt.setString(5, identityInfo.getId_type());
				pstmt.addBatch();
				if ((i + 1) == identityInfoList.size()) {
					pstmt.executeBatch();

				}
			}
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data Identity Info in table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (success) {
						conn.commit();
					} else {
						conn.rollback();
					}
					conn.close();
				}
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}
	}

	/**
	 * INSERT BUREAU ADDRESS DETAIL INFORMATION
	 * 
	 * @param bureauAddressList
	 * @param bureauAutoID
	 */
	public void insertDataToBureauAddressInformation(List<BureauAddressInformationModel> bureauAddressList,
			Integer bureauAutoID) {
		boolean success = true;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";

		try {
			query = "insert into bureau_address_info(bureau_report_data_id,address,state,postal_code,type) VALUES(?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(query);
			for (int i = 0; i < bureauAddressList.size(); i++) {
				BureauAddressInformationModel bureauAddress = (BureauAddressInformationModel) bureauAddressList.get(i);

				pstmt.setInt(1, bureauAutoID);
				pstmt.setString(2, bureauAddress.getAddress());
				pstmt.setString(3, bureauAddress.getState());
				pstmt.setInt(4, bureauAddress.getPostalCode());
				pstmt.setString(5, bureauAddress.getType());
				pstmt.addBatch();
				if ((i + 1) == bureauAddressList.size()) {
					pstmt.executeBatch();
				}
			}
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data Address Info in table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (success) {
						conn.commit();
					} else {
						conn.rollback();
					}
					conn.close();

				}
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}
	}

	/**
	 * INSERT BUREAU CONTACT INFORMATION
	 * 
	 * @param contactList
	 * @param bureauAutoID
	 */
	public void insertDataToBureauContactInformation(List<BureauContactInformationModel> contactList,
			Integer bureauAutoID) {
		boolean success = true;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";

		try {

			query = "insert into bureau_contact_info(bureau_report_data_id,contact_no,contact_type_code,contact_seq,"
					+ "contact_reported_date,area_code,country_code,contact_no_extension) VALUES(?,?,?,?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(query);
			for (int i = 0; i < contactList.size(); i++) {
				BureauContactInformationModel bureauContact = (BureauContactInformationModel) contactList.get(i);

				pstmt.setInt(1, bureauAutoID);
				pstmt.setString(2, bureauContact.getContactNo());
				pstmt.setString(3, bureauContact.getContactTypeCode());
				pstmt.setInt(4, bureauContact.getContactSeq());
				pstmt.setString(5, bureauContact.getContactReportedDate());
				pstmt.setString(6, bureauContact.getAreaCode());
				pstmt.setString(7, bureauContact.getCountryCode());
				pstmt.setString(8, bureauContact.getContactNoExtension());
				pstmt.addBatch();
				if ((i + 1) == contactList.size()) {
					pstmt.executeBatch();

				}
			}
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data contact Info in table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (success) {
						conn.commit();
					} else {
						conn.rollback();
					}
					conn.close();

				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	/**
	 * INSERT BUREAU EMAIL INFORMATION
	 * 
	 * @param bureauEmailInfo
	 * @param bureauAutoID
	 */
	public void insertDataToBureauEmailAddressInfo(BureauEmailAddressInformationModel bureauEmailInfo,
			int bureauAutoID) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";

		try {
			query = "insert into bureau_email_address_info(bureau_report_data_id,email_address) VALUES(?,?)";
			conn = ConnectionUtil.getConnection();
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, bureauAutoID);
			pstmt.setString(2, bureauEmailInfo.getEmailAddress());
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data Email Info in table-->>");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}
	}

	/**
	 * INSERT BUREAU ACCOUNT SUMMARY
	 * 
	 * @param bureauAccountSummaryInfo
	 * @param bureauAutoID
	 */
	public void insertDataToBureauAccountSummary(BureauAccountSummaryModel bureauAccountSummaryInfo,
			Integer bureauAutoID) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";

		try {

			query = "insert into bureau_account_summary(bureau_report_data_id,no_of_accounts,no_of_active_accounts,"
					+ "no_of_write_offs,total_past_due,most_severe_status_within_24_months,single_highest_credit,"
					+ "single_highest_sanction_amount,total_high_credit,avg_open_bal,single_highest_bal,no_of_past_due_accounts,"
					+ "no_of_zero_bal_accounts,recent_account,oldest_account,total_bal_amount,total_sanction_amount,"
					+ "total_credit_limit,total_monthly_payment_amount) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			pstmt = conn.prepareStatement(query);

			pstmt.setInt(1, bureauAutoID);
			pstmt.setInt(2, bureauAccountSummaryInfo.getNoOfAccounts());
			pstmt.setInt(3, bureauAccountSummaryInfo.getNoOfActiveAccounts());
			pstmt.setInt(4, bureauAccountSummaryInfo.getNoOfWriteOffs());
			pstmt.setString(5, bureauAccountSummaryInfo.getTotalPastDue());
			pstmt.setString(6, bureauAccountSummaryInfo.getMostSevereStatusWithin24Months());
			pstmt.setString(7, bureauAccountSummaryInfo.getSingleHighestCredit());
			pstmt.setString(8, bureauAccountSummaryInfo.getSingleHighestSanctionAmount());
			pstmt.setString(9, bureauAccountSummaryInfo.getTotalHighCredit());
			pstmt.setString(10, bureauAccountSummaryInfo.getAvgOpenBal());
			pstmt.setString(11, bureauAccountSummaryInfo.getSingleHighestBal());
			pstmt.setInt(12, bureauAccountSummaryInfo.getNoOfPastDueAccounts());
			pstmt.setInt(13, bureauAccountSummaryInfo.getNoOfZeroBalAccounts());
			pstmt.setString(14, bureauAccountSummaryInfo.getRecentAccount());
			pstmt.setString(15, bureauAccountSummaryInfo.getOldestAccount());
			pstmt.setString(16, bureauAccountSummaryInfo.getTotalBalAmount());
			pstmt.setString(17, bureauAccountSummaryInfo.getTotalSanctionAmount());
			pstmt.setString(18, bureauAccountSummaryInfo.getTotalCreditLimit());
			pstmt.setString(19, bureauAccountSummaryInfo.getTotalMonthlyPaymentAmount());
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data Account Summary Info in table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}
	}

	/**
	 * INSERT BUREAU RECENT ACTIVITY INFORMATION
	 * 
	 * @param bureauRecentActivityInfo
	 * @param bureauAutoID
	 */
	public void insertDataToBureauRecentActivityInfo(BureauRecentActivityInfoModel bureauRecentActivityInfo,
			Integer bureauAutoID) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";

		try {
			query = "insert into bureau_recent_activity_info(bureau_report_data_id,accounts_deliquent,accounts_opened,total_enquiries,"
					+ "accounts_updated) VALUES(?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			pstmt = conn.prepareStatement(query);

			pstmt.setInt(1, bureauAutoID);
			pstmt.setInt(2, bureauRecentActivityInfo.getAccountsDeliquent());
			pstmt.setInt(3, bureauRecentActivityInfo.getAccountsOpened());
			pstmt.setInt(4, bureauRecentActivityInfo.getTotalEnquiries());
			pstmt.setInt(5, bureauRecentActivityInfo.getAccountsUpdated());
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data Recent Activity Info in table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}
	}

	/**
	 * INSERT BUREAU OTHER KEY IDENTIFICATION INFORMATION
	 * 
	 * @param bureauOtherKeyIdentification
	 * @param bureauAutoID
	 */
	public void insertDataToBureauOtherKeyIdentification(BureauOtherKeyIdentificationModel bureauOtherKeyIdentification,
			int bureauAutoID) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";

		try {
			query = "insert into bureau_other_key_identification(bureau_report_data_id,age_of_oldest_trade,no_of_open_trades,all_lines_ever_written,all_lines_ever_written_in_9_months,"
					+ "all_lines_ever_written_in_6_months) VALUES(?,?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			pstmt = conn.prepareStatement(query);

			pstmt.setInt(1, bureauAutoID);
			pstmt.setString(2, bureauOtherKeyIdentification.getAgeOfOldestTrade());
			pstmt.setString(3, bureauOtherKeyIdentification.getNoOfOpenTrades());
			pstmt.setString(4, bureauOtherKeyIdentification.getAllLinesEverWritten());
			pstmt.setString(5, bureauOtherKeyIdentification.getAllLinesEverWrittenIn9Months());
			pstmt.setString(6, bureauOtherKeyIdentification.getAllLinesEverWrittenIn6Months());
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println(
					"Error in Inserting Bureau data other key identification Info in table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}
	}

	/**
	 * INSERT BUREAU ENQUIRY SUMMARY INFORMATION
	 * 
	 * @param bureauEnquirySummary
	 * @param bureauAutoID
	 */
	public void insertDataToBureauEnquirySummary(BureauEnquirySummaryModel bureauEnquirySummary, Integer bureauAutoID) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";

		try {
			query = "insert into bureau_enquiry_summary(bureau_report_data_id,purpose,total,"
					+ "past_30_days,past_12_months,past_24_months,recent) VALUES(?,?,?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, bureauAutoID);
			pstmt.setString(2, bureauEnquirySummary.getPurpose());
			pstmt.setString(3, bureauEnquirySummary.getTotal());
			pstmt.setString(4, bureauEnquirySummary.getPast30Days());
			pstmt.setString(5, bureauEnquirySummary.getPast12Months());
			pstmt.setString(6, bureauEnquirySummary.getPast24Months());
			pstmt.setString(7, bureauEnquirySummary.getRecent());

			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data enquiry summary Info in table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}
	}

	/**
	 * INSERT BUREAU ENQUIRY INFORMATION
	 * 
	 * @param bureauEnquiresInfo
	 * @param bureauAutoID
	 */
	public void insertDataToBureauEnquiriesInfo(BureauEnquiresInfoModel bureauEnquiresInfo, Integer bureauAutoID) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";

		try {
			query = "insert into bureau_enquiries_info(bureau_report_data_id,sequence_number,institution,date,time,"
					+ "request_purpose,amount) VALUES(?,?,?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			pstmt = conn.prepareStatement(query);

			pstmt.setInt(1, bureauAutoID);
			pstmt.setInt(2, bureauEnquiresInfo.getSequenceNumber());
			pstmt.setString(3, bureauEnquiresInfo.getInstitution());
			pstmt.setString(4, bureauEnquiresInfo.getDate());
			pstmt.setString(5, bureauEnquiresInfo.getTime());
			pstmt.setString(6, bureauEnquiresInfo.getRequestPurpose());
			pstmt.setBigDecimal(7, bureauEnquiresInfo.getAmount());
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data enquiry Info in table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}
	}

	/**
	 * INSERT BUREAU ACCOUNT DETAILS INFORMATION
	 * 
	 * @param bureauAccountDetails
	 * @param bureauAutoID
	 * @return
	 */
	public Integer insertDataToBureauAccountDetails(BureauAccountDetailsModel bureauAccountDetails,
			Integer bureauAutoID) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";
		Integer value = 0;
		try {
			query = "insert into bureau_accounts_details(bureau_report_data_id,account_no,institution_name,account_type,ownership_type,balance,past_due_amount,last_payment,"
					+ "open,sanction_amount,last_payment_date,date_reported,date_opened,reason,intrest_rate,repayment_tenure,dispute_code,installment_amount,account_seq_no,account_reported_date,"
					+ "collateral_type,collateral_value,date_closed,account_status,asset_classification,suit_filed_status,high_credit,credit_limit,term_frequency) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, bureauAutoID);
			pstmt.setString(2, bureauAccountDetails.getAccountNo());
			pstmt.setString(3, bureauAccountDetails.getInstitutionName());
			pstmt.setString(4, bureauAccountDetails.getAccountType());
			pstmt.setString(5, bureauAccountDetails.getOwnershipType());
			pstmt.setBigDecimal(6, bureauAccountDetails.getBalance());
			pstmt.setBigDecimal(7, bureauAccountDetails.getPastDueAmount());
			pstmt.setBigDecimal(8, bureauAccountDetails.getLastPayment());
			pstmt.setString(9, bureauAccountDetails.getOpen());
			pstmt.setBigDecimal(10, bureauAccountDetails.getSanctionAmount());
			pstmt.setString(11, bureauAccountDetails.getLastPaymentDate());
			pstmt.setString(12, bureauAccountDetails.getDateReported());
			pstmt.setString(13, bureauAccountDetails.getDateOpened());
			pstmt.setString(14, bureauAccountDetails.getReason());
			pstmt.setString(15, bureauAccountDetails.getIntrestRate());
			pstmt.setString(16, bureauAccountDetails.getRepaymentTenure());
			pstmt.setString(17, bureauAccountDetails.getDisputeCode());
			pstmt.setBigDecimal(18, bureauAccountDetails.getInstallmentAmount());
			pstmt.setInt(19, bureauAccountDetails.getAccountSeqNo());
			pstmt.setString(20, bureauAccountDetails.getAccountReportedDate());
			pstmt.setString(21, bureauAccountDetails.getCollateralType());
			pstmt.setString(22, bureauAccountDetails.getCollateralValue());
			pstmt.setString(23, bureauAccountDetails.getDateClosed());
			pstmt.setString(24, bureauAccountDetails.getAccountStatus());
			pstmt.setString(25, bureauAccountDetails.getAssetClassification());
			pstmt.setString(26, bureauAccountDetails.getSuitFiledStatus());
			pstmt.setBigDecimal(27, bureauAccountDetails.getHighCredit());
			pstmt.setBigDecimal(28, bureauAccountDetails.getCreditLimit());
			pstmt.setString(29, bureauAccountDetails.getTermFrequency());
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				value = rs.getInt(1);
				System.out.println("Auto Incremented Bureau account detail Id-->> " + value);
			}
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data enquiry Info in table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}
		return value;
	}

	/**
	 * INSERT BUREAU 48 MONTH HISTORY INFORMATION
	 * 
	 * @param month48list
	 * @param bureauAccountID
	 */
	public void insertDataToBureauHistory48Months(List<BureauHistory48MonthsModel> month48list,
			Integer bureauAccountID) {
		boolean success = true;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";
		try {
			query = "INSERT INTO bureau_history_48_months(bureau_account_details_id,month_key,"
					+ "payment_status,suit_filed_status,asset_classifiation_status) VALUES(?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(query);
			for (int i = 0; i < month48list.size(); i++) {
				BureauHistory48MonthsModel month48info = (BureauHistory48MonthsModel) month48list.get(i);

				pstmt.setInt(1, bureauAccountID);
				pstmt.setString(2, month48info.getMonthKey());
				pstmt.setString(3, month48info.getPaymentStatus());
				pstmt.setString(4, month48info.getSuitFiledStatus());
				pstmt.setString(5, month48info.getAssetClassifiationStatus());

				pstmt.addBatch();
				if ((i + 1) == month48list.size()) {
					pstmt.executeBatch();

				}
			}
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data 48 month Info in table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {

					if (success) {
						conn.commit();
					} else {
						conn.rollback();
					}
					conn.close();

				}
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}
	}

	/**
	 * GET CUSTOMER BASIC INFORMATION FROM ITS SUBSCRIBER ID
	 * 
	 * @param subscriberId
	 * @return
	 */
	public ArrayList<BureauSubscriberDetailsModel> getSubsId(int subscriberId) {
		ArrayList<BureauSubscriberDetailsModel> bureauSubsDetailsList = new ArrayList<BureauSubscriberDetailsModel>();
		Connection connection = null;
		
		//Statement statement = null;
		// int sbusId=0;
		String getSubscriberDetails = "SELECT subs_id,first_name,last_name,gender,marital_status,mobile_number,address,state,city,pincode,date_of_birth,address2 from tbl_subscribers where subs_id="
				+ subscriberId;
		try {
			connection = ConnectionUtil.getConnection();
			//connection.setAutoCommit(false);
			//statement = connection.createStatement();
			// Logger.getLogger("Bureau").debug("SubscriberDetails : " +
			// getSubscriberDetails);
			BureauSubscriberDetailsModel bureauSubscriberDetails = null;
			PreparedStatement preparedStatement = connection.prepareStatement(getSubscriberDetails);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				System.out.println("BureauDBDao first name::::"+resultSet.getString("first_name"));
				bureauSubscriberDetails = new BureauSubscriberDetailsModel();
				bureauSubscriberDetails.setSubscriberId(resultSet.getInt("subs_id"));
				bureauSubscriberDetails.setAddress(resultSet.getString("address"));
				bureauSubscriberDetails.setFirstName(resultSet.getString("first_name"));
				bureauSubscriberDetails.setLastName(resultSet.getString("last_name"));
				bureauSubscriberDetails.setMaritalStatus(resultSet.getString("marital_status"));
				bureauSubscriberDetails.setGender(resultSet.getString("gender"));
				bureauSubscriberDetails.setPincode(resultSet.getInt("pincode"));
				bureauSubscriberDetails.setCity(resultSet.getString("city"));
				bureauSubscriberDetails.setMobileNumber(resultSet.getString("mobile_number"));
				bureauSubscriberDetails.setState(resultSet.getString("state"));
				bureauSubscriberDetails.setDateOfBirth(resultSet.getString("date_of_birth"));
				bureauSubscriberDetails.setAddress2(resultSet.getString("address2"));
				bureauSubsDetailsList.add(bureauSubscriberDetails);
			}
		} catch (Exception e) {
			System.out.println("Error in getting getSubsId details--->");
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
				if (connection != null)
					connection.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return bureauSubsDetailsList;

	}

	/**
	 * FETCH CUSTOMER PAN CARD NUMBER FILLED BY CUSTOMER IN APPLICATION
	 * 
	 * @param subscriberId
	 * @return
	 */
	public String getPANId(int subscriberId) {
		System.out.println("subscriberId in getPANId  " + subscriberId);

		String PANCardId = null;
		Connection connection = null;
		Statement statement = null;
		String getPANId = "SELECT Id_number from tbl_subs_idproof_details id where id.tbl_subscribers_subs_id="
				+ subscriberId + " and id.tbl_id_proof_proof_id=" + 8
				+ " AND id.Id_number IS NOT NULL ORDER BY id.subs_proof_id DESC LIMIT 1";

		System.out.println("Query getPANId	" + getPANId);
		try {
			connection = ConnectionUtil.getConnection();
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(getPANId);
			if (resultSet != null) {
				if (resultSet.next()) {
					if (resultSet.getString("Id_number") != null) {
						PANCardId = resultSet.getString("Id_number");
					} else {
						PANCardId = "Pan Id Not Available";
					}
				}
			}

		} catch (Exception e) {
			System.out.println("Error in getPANId--->");
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}

		}
		return PANCardId;
	}

	/**
	 * CHECK CUSTOMER PAN NUMBER IN BUREAU REPORT RESPONSE TABLE FROM ITS PANCARD
	 * NUMBER
	 * 
	 * @param panId
	 * @param subscriberId
	 * @return
	 */
	public String getPANIdFromBureau(String panId, int subscriberId) {
		System.out.println("Pan Id	" + panId);
		System.out.println("subscriber Id	" + subscriberId);

		String PANCardIdBureau = null;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String getPANIdBureau = "SELECT pan_id from bureau_report_response_data where subs_id=" + subscriberId;
		try {
			connection = ConnectionUtil.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(getPANIdBureau);
			if (resultSet != null) {
				if (resultSet.next()) {
					PANCardIdBureau = resultSet.getString("pan_id");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return PANCardIdBureau;
	}

	/**
	 * FETCH SUBSCRIBERS ID FROM BUREAU REPORT RESPONSE DATA FROM CUSTOMER PAN
	 * NUMBER
	 * 
	 * @param panId
	 * @return
	 */
	public BureauReportResponseData checkBureauPanCard(String panId, int subs_id) { 
		System.out.println("checkBureauPanCard Pan Id	" + panId);
		BureauReportResponseData responseData = null;

		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String getPANIdBureau = "SELECT subs_id,score_value,scoring_element_code from bureau_report_response_data where pan_id='"
				+ panId + "' and subs_id=" + subs_id + " ORDER BY bureau_report_data_id DESC LIMIT 1";
		try {
			connection = ConnectionUtil.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(getPANIdBureau);
			if (resultSet.next()) {
				responseData = new BureauReportResponseData();
				responseData.setSubs_id(resultSet.getInt("subs_id"));
				responseData.setScoreValue(resultSet.getString("score_value"));
				responseData.setScoringElementCode(resultSet.getString("scoring_element_code"));
			}
		} catch (Exception e) {
			System.out.println("Exception in checkBureauPanCard--->");
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();

				if (connection != null)
					connection.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return responseData;
	}
	
	public Integer checkHitpendingStatus(String panId) { 
		System.out.println("Check Pending status Pan Id	" + panId);
		Integer customerSubsId = 0;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String getPANIdBureau = "SELECT COUNT(1) from bureau_request where req_state='pending' and pan_id='" + panId
				+ "' order by bureau_req_id desc limit 1";
		try {
			connection = ConnectionUtil.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(getPANIdBureau);
			if (resultSet.next()) {
				customerSubsId = resultSet.getInt("COUNT(1)");
			}
		} catch (Exception e) {
			System.out.println("Exception in checkHitpendingStatus------->");
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();

				if (connection != null)
					connection.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return customerSubsId;
	}
	
	/**
	 * INSERT CUSTOMER BUREAU REQUEST LOG
	 * 
	 * @param requestBody
	 * @param panId
	 * @param pincode
	 * @param dob
	 * @param subscriberId
	 * @return
	 */
	public Integer insertRequestLog(RequestBodyType requestBody, String panId, int pincode, String dob, int subscriberId) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";
		int count = 0;
		DateFormat format3 = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat format4 = new SimpleDateFormat("HH:mm:ss");
		format3.setTimeZone(TimeZone.getTimeZone("IST"));
		format4.setTimeZone(TimeZone.getTimeZone("IST"));
		try {
			query = "INSERT INTO bureau_request(subs_id,first_name,last_name,date_of_birth,address,state,pincode,pan_id,mobile_number,req_status,create_date,create_time) "
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			pstmt.setInt(1, subscriberId);
			pstmt.setString(2, requestBody.getFirstName());
			pstmt.setString(3, requestBody.getLastName());
			pstmt.setString(4, dob);
			pstmt.setString(5, requestBody.getAddrLine1());
			pstmt.setString(6, requestBody.getState().value());
			pstmt.setInt(7, pincode);
			pstmt.setString(8, panId);
			pstmt.setString(9, requestBody.getMobilePhone());
			pstmt.setString(10, "online");
			pstmt.setString(11, format3.format(new Date()));
			pstmt.setString(12, format4.format(new Date()));
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				count = rs.getInt(1);

			}
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data in master table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}
		return count;

	}
	
	/**
	 * INSERT BUREAU SCORE ELEMENTS
	 * 
	 * @param bureauScoringElementsModel
	 * @param bureauAutoID
	 */
	public void insertDataToBureauScoreElements(BureauScoringElementsModel bureauScoringElementsModel,
			int bureauAutoID) {

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			String query = "insert into bureau_score_elements(bureau_report_data_id,score_type,score_sequence,score_code,score_desc"
					+ ") VALUES(?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			pstmt = conn.prepareStatement(query);

			pstmt.setInt(1, bureauAutoID);
			pstmt.setString(2, bureauScoringElementsModel.getScore_type());
			pstmt.setInt(3, bureauScoringElementsModel.getScore_sequence());
			pstmt.setString(4, bureauScoringElementsModel.getScore_code());
			pstmt.setString(5, bureauScoringElementsModel.getScore_desc());

			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data score element info in table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage());
				e2.printStackTrace();
			}
		}
	}
	
	/**
	 * UPDATE BUREAU REQUEST LOG EITHER SUCCESS OR FAILURE
	 * 
	 * @param req_id
	 * @param pan_id
	 * @param successStatus
	 */
	public void updateRequestStatus(int req_id, String pan_id, String successStatus) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";

		try {
			
			conn = ConnectionUtil.getConnection();
			query = "UPDATE bureau_request set req_state=? WHERE bureau_req_id=? and pan_id=? ";
			pstmt = conn.prepareStatement(query);

			pstmt.setString(1, successStatus);
			pstmt.setInt(2, req_id);
			pstmt.setString(3, pan_id);

			pstmt.executeUpdate();
			System.out.println("update " + successStatus + " status successfully"); 
		} catch (Exception e) {
			System.out.println("Error in update bureau request log status in table-->> " + pan_id + " ---  ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println(e2.getMessage());
				e2.printStackTrace();
			}
		}
	}
	
	/**
	 * CHEACK CUSTOMER DETAILS IN BUREAU REQUEST, BUREAU IS REQUESTED OR NOT
	 * 
	 * @param panId
	 * @param subs_id
	 * @return
	 */
	public BureauReqLogModel checkBureauSubsId(String panId, int subs_id) {
		BureauReqLogModel bureauReq = null;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String getPANIdBureau = "SELECT subs_id,create_date,req_state from bureau_request where pan_id='" + panId
				+ "' AND subs_id=" + subs_id + "  ORDER BY bureau_req_id desc LIMIT 1 ";
		try { 
			System.out.println("query for pan id :: " + getPANIdBureau);
			connection = ConnectionUtil.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(getPANIdBureau);
			bureauReq = new BureauReqLogModel();
			if (resultSet != null) {
				if (resultSet.next()) {
					bureauReq.setSubsId(resultSet.getInt("subs_id"));
					bureauReq.setCreateDate(resultSet.getString("create_date"));
					bureauReq.setReqState(resultSet.getString("req_state"));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return bureauReq;
	}
	
	/**
	 * INSERT BUREAU TRANSACTION LOG AT BUREAU HIT
	 * 
	 * @param bureaulog
	 * @return
	 */
	public Integer insertBureauRequestLog(BureauReqLogModel bureaulog) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";
		int count = 0;
		DateFormat format3 = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat format4 = new SimpleDateFormat("HH:mm:ss");
		format3.setTimeZone(TimeZone.getTimeZone("IST"));
		format4.setTimeZone(TimeZone.getTimeZone("IST"));
		try {
			query = "INSERT INTO bureau_request(subs_id,first_name,last_name,date_of_birth,address,state,pincode,pan_id,mobile_number,req_status,create_date,create_time) "
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			pstmt.setInt(1, bureaulog.getSubsId());
			pstmt.setString(2, bureaulog.getFirstName());
			pstmt.setString(3, bureaulog.getLastName());
			pstmt.setString(4, bureaulog.getDateOfBirth());
			pstmt.setString(5, bureaulog.getAddress());
			pstmt.setString(6, bureaulog.getState());
			pstmt.setInt(7, bureaulog.getPincode());
			pstmt.setString(8, bureaulog.getPanId());
			pstmt.setLong(9, bureaulog.getMobileNumber());
			pstmt.setString(10, "online");
			pstmt.setString(11, bureaulog.getCreateDate());
			pstmt.setString(12, bureaulog.getCreateTime());

			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				count = rs.getInt(1);

			}
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data in master table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage()); 
				e2.printStackTrace();
			}
		}
		return count;

	}
	
	/**
	 * INSERT INTO BUREAU RESPORT RESPONSE DATA AFTER BUREAU HIT
	 * 
	 * @param BureauData
	 * @param subs_id
	 * @param bureauReqId
	 * @return
	 */
	public Integer insertBureauResponseData(BureauReportResponseData BureauData, int subs_id, int bureauReqId) {

		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";
		int count = 0;
		try {
			query = "insert into bureau_report_response_data(subs_id,report_order_no,product_code,success_code,date,time,hit_code,"
					+ "customer_code,client_id,inquiry_purpose,address_line,state,postal_code,phone_no,phone_type,pan_id,"
					+ "score_name,score_value,customer_ref_field,scoring_element_type,scoring_element_seq,scoring_element_code,"
					+ "scoring_element_desc,score_reason_code,score_desc,bureau_master_id,bureau_trans_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			conn = ConnectionUtil.getConnection();

			pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, subs_id);
			pstmt.setString(2, BureauData.getReportOrderNo());
			pstmt.setString(3, BureauData.getProductCode());
			pstmt.setString(4, BureauData.getSuccessCode());
			pstmt.setString(5, BureauData.getDate());
			pstmt.setString(6, BureauData.getTime());
			pstmt.setString(7, BureauData.getHitCode());
			pstmt.setString(8, BureauData.getCustomerCode());
			pstmt.setString(9, BureauData.getClientId());
			pstmt.setString(10, BureauData.getInquiryPurpose());
			pstmt.setString(11, BureauData.getAddressLine());
			pstmt.setString(12, BureauData.getState());
			pstmt.setString(13, BureauData.getPostalCode());
			pstmt.setString(14, BureauData.getPhoneNo());
			pstmt.setString(15, BureauData.getPhoneType());
			pstmt.setString(16, BureauData.getPanId());
			pstmt.setString(17, BureauData.getScoreName());
			pstmt.setString(18, BureauData.getScoreValue());
			pstmt.setString(19, BureauData.getCustomerRefField());
			pstmt.setString(20, BureauData.getScoringElementType());
			pstmt.setInt(21, BureauData.getScoringElementSeq());
			pstmt.setString(22, BureauData.getScoringElementCode());
			pstmt.setString(23, BureauData.getScoringElementDesc());
			pstmt.setString(24, BureauData.getScoreReasonCode());
			pstmt.setString(25, BureauData.getScoreDesc());
			pstmt.setInt(26, 1);
			pstmt.setInt(27, bureauReqId);
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				count = rs.getInt(1);
				System.out.println("Auto Incremented Bureau Id-->> " + count + " and subscriber id is-->> " + subs_id); 
			}
		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data in master table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage()); 
				e2.printStackTrace();
			}
		}
		return count;

	}
	
	public BureauReqLogModel checkBureauPandIdInBureau(String panId) {
		BureauReqLogModel bureauReq = null;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		String getPANIdBureau = "SELECT subs_id,create_date,req_state from bureau_request where pan_id='" + panId
				+ "'  ORDER BY bureau_req_id desc LIMIT 1 ";
		try {
			System.out.println("query for pan id :: " + getPANIdBureau);
			connection = ConnectionUtil.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(getPANIdBureau);
			bureauReq = new BureauReqLogModel();
			if (resultSet != null) {
				if (resultSet.next()) {
					bureauReq.setSubsId(resultSet.getInt("subs_id"));
					bureauReq.setCreateDate(resultSet.getString("create_date"));
					bureauReq.setReqState(resultSet.getString("req_state"));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();

				if (connection != null)
					connection.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return bureauReq;
	}
	
	public BureauSubscriberDetailsModel getSubscriberDetail(int subscriberId) {
		BureauSubscriberDetailsModel bureauSubscriberDetails = null;
		Connection connection = null;
		Statement statement = null;
		// int sbusId=0;
		String getSubscriberDetails = "SELECT subs_id,first_name,last_name,gender,marital_status,mobile_number,address,state,city,pincode,date_of_birth,address2 from tbl_subscribers where subs_id="
				+ subscriberId;
		try {
			connection = ConnectionUtil.getConnection();
			statement = connection.createStatement();
			System.out.println("SubscriberDetails	: " + getSubscriberDetails);

			ResultSet resultSet = statement.executeQuery(getSubscriberDetails);
			if (resultSet != null) {
				if (resultSet.next()) {
					bureauSubscriberDetails = new BureauSubscriberDetailsModel();
					bureauSubscriberDetails.setSubscriberId(resultSet.getInt("subs_id"));
					bureauSubscriberDetails.setAddress(resultSet.getString("address"));
					bureauSubscriberDetails.setFirstName(resultSet.getString("first_name"));
					bureauSubscriberDetails.setLastName(resultSet.getString("last_name"));
					bureauSubscriberDetails.setMaritalStatus(resultSet.getString("marital_status"));
					bureauSubscriberDetails.setGender(resultSet.getString("gender"));
					bureauSubscriberDetails.setPincode(resultSet.getInt("pincode"));
					bureauSubscriberDetails.setCity(resultSet.getString("city"));
					bureauSubscriberDetails.setMobileNumber(resultSet.getString("mobile_number"));
					bureauSubscriberDetails.setState(resultSet.getString("state"));
					bureauSubscriberDetails.setDateOfBirth(resultSet.getString("date_of_birth"));
					bureauSubscriberDetails.setAddress2(resultSet.getString("address2"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return bureauSubscriberDetails;

	}
	
	public BureauReportResponseData fetchCustomerBureauDetails(String panId, int subs_id) { 
		System.out.println("Pan Id	" + panId);
		BureauReportResponseData bureauData = null;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String getPANIdBureau = "SELECT subs_id,pan_id,date from bureau_report_response_data where pan_id='" + panId
				+ "' and subs_id=" + subs_id + " order by bureau_report_data_id DESC limit 1";
		try {
			connection = ConnectionUtil.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(getPANIdBureau);
			if (resultSet != null) {
				if (resultSet.next()) {
					bureauData = new BureauReportResponseData();
					bureauData.setSubs_id(resultSet.getInt("subs_id"));
					bureauData.setPanId(resultSet.getString("pan_id"));
					bureauData.setDate(resultSet.getString("date"));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return bureauData;
	}

	public BankAccountDetailsModel getBankName(int subs_id) {
		BankAccountDetailsModel bankAccountDetailsModel = null;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		String getPANIdBureau = "SELECT REPLACE(IF(INSTR(first_name,'M/S') || INSTR(first_name,'MR.') || INSTR(first_name,'MRS') >0,"
				+ "TRIM(SUBSTR(first_name,4)),first_name),'.','') AS 'first_name',last_name,is_active FROM tbl_bank_account_details "
				+ "WHERE  tbl_subscribers_subs_id=" + subs_id
				+ " AND is_active='Y' AND first_name IS NOT NULL AND first_name !='' ORDER BY "
				+ "account_id DESC LIMIT 1";
		try {
			connection = ConnectionUtil.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(getPANIdBureau);
			if (resultSet.next()) {
				bankAccountDetailsModel = new BankAccountDetailsModel();
				bankAccountDetailsModel.setFirst_name(resultSet.getString("first_name"));
				bankAccountDetailsModel.setLast_name(resultSet.getString("last_name"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return bankAccountDetailsModel;
	}
	
	public void insertDimensionVariable(DimensionVariableModel dimensionVariableModel) {
		boolean success = true;
		Connection conn = null;
		PreparedStatement pstmt = null;
		String query = "";
		try {
			query = "INSERT INTO bureau_dimension_variable(bureau_report_data_id,tda_mesmi_cc_psdamt_24,"
					+ "tda_mesme_ins_psdamt_24,tda_metsu_cc_psdamt_3,tda_sum_pf_psdamt_3) VALUES(?,?,?,?,?)";
			conn = ConnectionUtil.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(query);

			pstmt.setInt(1, dimensionVariableModel.getBureauId());
			pstmt.setDouble(2, dimensionVariableModel.getTDA_MESMI_CC_PSDAMT_24());
			pstmt.setDouble(3, dimensionVariableModel.getTDA_MESME_INS_PSDAMT_24());
			pstmt.setDouble(4, dimensionVariableModel.getTDA_METSU_CC_PSDAMT_3());
			pstmt.setDouble(5, dimensionVariableModel.getTDA_SUM_PF_PSDAMT_3());
			pstmt.executeUpdate();
			;

		} catch (Exception e) {
			System.out.println("Error in Inserting Bureau data Dimensional variable Info in table-->> ");
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {

					if (success) {
						conn.commit();
					} else {
						conn.rollback();
					}
					conn.close();

				}
			} catch (Exception e2) {
				System.out.println("Error are comming! -->  " + e2.getMessage()); 
				e2.printStackTrace();
			}
		}
	}
	
	public Long getCustomerCode(int subsId) {

		Connection connection = null;
		Statement statement = null;
		Long customerCode = 0L;
		String getSubscriberDetails = "SELECT customer_code from tbl_subscribers where subs_id=" + subsId;
		try {
			connection = ConnectionUtil.getConnection();
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(getSubscriberDetails);
			if (resultSet.next()) {
				customerCode = resultSet.getLong("customer_code");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return customerCode;

	}
	
	public BureauVo getCirDetails(BureauVo vo, Integer subs_id) {
		Connection connection = null;
		Statement statement = null;
		System.out.println("id "+subs_id);
		String getCirDetails = "SELECT bureau_report_data_id,scoring_element_code,score_value FROM bureau_report_response_data  WHERE subs_id= "
				+ subs_id + " ORDER BY bureau_report_data_id DESC LIMIT 1";
		try {
			connection = ConnectionUtil.getConnection();
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(getCirDetails);
			if (resultSet.next()) {
				System.out.println("value  "+resultSet.getString("score_value"));
				vo.setCir(resultSet.getString("score_value"));
				vo.setScoreElement(resultSet.getString("scoring_element_code"));
				vo.setBureauId(resultSet.getInt("bureau_report_data_id"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return vo;

	}
}
