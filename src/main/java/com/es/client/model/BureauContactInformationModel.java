/**
 * 
 */
package com.es.client.model;


import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauContactInformationModel {
	private int bureauContactInfoId;
	private int bureauReportDataId;
	private String contactNo;
	private String contactTypeCode;
	private int contactSeq;
	private String contactReportedDate;
	private String areaCode;
	private String countryCode;
	private String contactNoExtension;
}
