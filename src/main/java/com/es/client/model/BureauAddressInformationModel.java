package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauAddressInformationModel {
private int bureauAddressInfoId;
	private int bureauReportDataId;
	private String address;
	private String state;
	private int postalCode;
	private String type;
}
