/**
 * 
 */
package com.es.client.model;


import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauIdentityInformationModel {
	private int bureauIdentityInfoId;
	private int bureauReportDataId;
	private String uniqueIdNo;
	private int uniqueIdSeq;
	private String uniqueIdReportedDate;
	private String id_type;
}
