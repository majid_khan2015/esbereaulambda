/**
 * 
 */
package com.es.client.model;

import java.math.BigDecimal;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauAccountDetailsModel {
	private int bureauAccountDetailsId;
	private int bureauReportDataId;
	private String accountNo;
	private String institutionName;
	private String accountType;
	private String ownershipType;
	private BigDecimal balance;
	private BigDecimal pastDueAmount;
	private BigDecimal lastPayment;
	private String open;
	private BigDecimal sanctionAmount;
	private String lastPaymentDate;
	private String dateReported;
	private String dateOpened;
	private String reason;
	private String intrestRate;
	private String repaymentTenure;
	private String disputeCode;
	private int accountSeqNo;
	private String accountReportedDate;
	private String termFrequency;
	private String collateralType;
	private String collateralValue;
	private String dateClosed;
	private String accountStatus;
	private String assetClassification;
	private String suitFiledStatus;
	private BigDecimal installmentAmount;
	private BigDecimal highCredit;
	private BigDecimal creditLimit;
}
