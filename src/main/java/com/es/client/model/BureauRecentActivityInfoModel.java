/**
 * 
 */
package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauRecentActivityInfoModel {
	private int bureauRecentActivityId;
	private int bureauReportDataId;
	private int accountsDeliquent;
	private int accountsOpened;
	private int totalEnquiries;
	private int accountsUpdated;
}
