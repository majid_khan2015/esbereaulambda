/**
 * 
 */
package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class DimensionVariableModel {
	private Integer dimensionalVariableId;
	private Integer bureauId;
	private Double TDA_MESMI_CC_PSDAMT_24;
	private Double TDA_MESME_INS_PSDAMT_24;
	private Double TDA_METSU_CC_PSDAMT_3;
	private Double TDA_SUM_PF_PSDAMT_3;
}
