/**
 * 
 */
package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class FoirResponseModel {

	private Integer subs_id;
	private Double salary;
	private Integer request_type;
	private Long customer_id;
	private Integer foir_value;
	private Integer status_code;
	private String status_desc;
	private String scoreElement;
}
