/**
 * 
 */
package com.es.client.model;


import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauOtherKeyIdentificationModel {
	private int bureauOtherKeyIndId;
	private int bureauReportDataId;
	private String ageOfOldestTrade;
	private String noOfOpenTrades;
	private String allLinesEverWritten;
	private String allLinesEverWrittenIn9Months;
	private String allLinesEverWrittenIn6Months;
}
