/**
 * 
 */
package com.es.client.model;


import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauEmailAddressInformationModel {
	private int bureauEmailAddressId;
	private int bureauReportDataId;
	private String emailAddress;
}
