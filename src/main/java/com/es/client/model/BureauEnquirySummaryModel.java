/**
 * 
 */
package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauEnquirySummaryModel {
	private int bureauEnquirySummaryId;
	private int bureauReportDataId;
	private String purpose;
	private String total;
	private String past30Days;
	private String past12Months;
	private String past24Months;
	private String recent;

}
