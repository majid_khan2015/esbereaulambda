/**
 * 
 */
package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauHistory48MonthsModel {
	private int history48MonthsId;
	private int bureauAccountDetailsId;
	private String monthKey;
	private String paymentStatus;
	private String suitFiledStatus;
	private String assetClassifiationStatus;
}
