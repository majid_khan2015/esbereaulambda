/**
 * 
 */
package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauAccountSummaryModel {

	private int bureauAccountSummaryId; 
	private int bureauReportDataId; 
	private int noOfAccounts; 
	private int noOfActiveAccounts;
	private int noOfWriteOffs;
	private String totalPastDue;
	private String mostSevereStatusWithin24Months;
	private String singleHighestCredit;
	private String singleHighestSanctionAmount; 
	private String totalHighCredit; 
	private String avgOpenBal; 
	private String singleHighestBal; 
	private int noOfPastDueAccounts; 
	private int noOfZeroBalAccounts; 
	private String recentAccount; 
	private String oldestAccount; 
	private String totalBalAmount; 
	private String totalSanctionAmount; 
	private String totalCreditLimit; 
	private String totalMonthlyPaymentAmount; 
	private String totalWrittenOffAmount;
}
