/**
 * 
 */
package com.es.client.model;
 

import lombok.Data;

/**
 * @author majidkhan
 *
 */
@Data
public class BureauReportResponseData {

	private Integer bureauReportDataId;
	private String reportOrderNo;
	private String productCode;
	private String successCode;
	private String date;
	private String time;
	private String hitCode;
	private String customerCode;
	private String clientId;
	private String inquiryPurpose;
	private String addressLine;
	private String state;
	private String postalCode;
	private String phoneNo;
	private String phoneType;
	private String panId;
	private String scoreName;
	private String scoreValue;
	private String customerRefField;
	private String scoringElementType;
	private Integer scoringElementSeq;
	private String scoringElementCode;
	private String scoringElementDesc;
	private String scoreReasonCode;
	private String scoreDesc;
	private Integer bureau_master_id;
	private Integer subs_id;
}
