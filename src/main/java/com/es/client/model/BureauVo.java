/**
 * 
 */
package com.es.client.model;


import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauVo {
	private String message;
	private Integer status;
	private Boolean is_success;
	private String status_code;
	private String cir;
	private Integer esScore;
	private Integer foir;
	private String scoreElement;
	private Long customer_id;
	private Integer subs_id;
	private Integer bureauId;
	private Double salary;
	

	
	
	
	
	
	
}
