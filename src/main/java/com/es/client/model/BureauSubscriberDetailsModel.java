/**
 * 
 */
package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauSubscriberDetailsModel {
	private Integer subscriberId; 
	private String firstName; 
	private String lastName;  
	private String gender; 
	private String maritalStatus; 
	private String mobileNumber; 
	private String address; 
	private String state; 
	private String city; 
	private Integer pincode; 
	private String dateOfBirth; 
	private String address2;
}
