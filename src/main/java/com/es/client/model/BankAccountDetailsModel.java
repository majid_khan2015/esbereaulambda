/**
 * 
 */
package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BankAccountDetailsModel {
	private String first_name;
	private String last_name;
	private String is_active;
}
