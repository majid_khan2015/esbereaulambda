/**
 * 
 */
package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauReqLogModel {
	private int bureau_req_id;
	private int subsId;
	private String firstName;
	private String lastName;
	private String dateOfBirth;
	private String address;
	private String state;
	private int pincode;
	private String panId;
	private long mobileNumber;
	private String reqState;
	private String reqStatus;
	private String createDate;
	private String createTime;
}
