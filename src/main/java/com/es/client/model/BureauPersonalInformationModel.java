/**
 * 
 */
package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauPersonalInformationModel {

	private int personalInfoId;
	private int bureauReportDataId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String dateOfBirth;
	private String gender;
	private String age;
	private String income;
	private String occupationType;
}
