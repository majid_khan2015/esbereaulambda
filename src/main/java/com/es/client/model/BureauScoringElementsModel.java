/**
 * 
 */
package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauScoringElementsModel {
	private Double score_details_id;
    private String score_type;
	private int score_sequence;
    private String score_code;	
    private String score_desc;
	private Double bureau_report_data_id;	
}
