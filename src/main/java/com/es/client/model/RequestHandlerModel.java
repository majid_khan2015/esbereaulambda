/**
 * 
 */
package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class RequestHandlerModel {
	private Integer bureauId;
	private Double salary;
	private Integer subscriberId;
}
