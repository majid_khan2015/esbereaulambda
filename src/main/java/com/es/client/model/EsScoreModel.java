/**
 * 
 */
package com.es.client.model;

import lombok.Data;



@Data
public class EsScoreModel {
	private String status;
	private Integer es_score;
	private String auto_decision;
	private Integer status_code;
}
