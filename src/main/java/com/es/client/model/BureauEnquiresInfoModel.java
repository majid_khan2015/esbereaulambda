/**
 * 
 */
package com.es.client.model;

import java.math.BigDecimal;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class BureauEnquiresInfoModel {
	private int bureauEnquriesId;
	private int bureauReportDataId;
	private int sequenceNumber;
	private String institution;
	private String date;
	private String time;
	private String requestPurpose;
	private BigDecimal amount;
}
