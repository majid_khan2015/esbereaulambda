/**
 * 
 */
package com.es.client.score;

import org.json.JSONObject;

import com.es.client.business.ExternalApiCall;
import com.es.client.model.BureauVo;
import com.es.client.model.EsScoreModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class ESScoreService {
	private Gson gsonObj = null;
	//private static String esApi = "http://172.31.21.224:6363/ScoringAutoAPI";
	private static final String esApi = System.getenv("esApi");

	public BureauVo getEsScore(BureauVo vo) {
		try {
			if (vo.getCustomer_id() != null) {
				// Request Creation For ES Score Call
				JSONObject esObject = setEsScorePara(vo); 
				System.out.println("Resquest sent for ES Score");
				String response = new ExternalApiCall().esApi(esObject, esApi);
				System.out.println("ES response ::  " + response); 
				this.gsonObj = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
				EsScoreModel esRes = (EsScoreModel) this.gsonObj.fromJson(response, EsScoreModel.class);
				System.out.println("EsScore Object fetched via API--->"+esRes);
				if (esRes.getStatus_code() == 200) {
					vo.setEsScore(esRes.getEs_score());
				}

			} else { 
				System.out.println("customer not found for ES score calculation");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return vo;
	}

	private JSONObject setEsScorePara(BureauVo vo) {
		JSONObject esRequest = new JSONObject();
		esRequest.put("customer_id", vo.getCustomer_id());
		esRequest.put("subs_id", vo.getSubs_id());
		esRequest.put("foir", vo.getFoir());
		esRequest.put("cir", vo.getCir());
		esRequest.put("scoreElement", vo.getScoreElement());
		esRequest.put("bureauId", vo.getBureauId());
		esRequest.put("salary", vo.getSalary());
		esRequest.put("req_type", 1);
		System.out.println("request json for es score :: " + esRequest.toString()); 
		return esRequest;
	}
}
