/**
 * 
 */
package com.es.client.business;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.es.client.util.ConnectionUtil;


/**
 * @author majidkhan
 *
 */
public class PincodeMappingDao {
	public String getPincodeState(String pincode) {
		StringWriter stack = new StringWriter();
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String query = "";
		String count = null;

		try {
			query = "select state_code from tbl_pincode_list a where a.area_pincode="+ pincode;
			conn =ConnectionUtil.getConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(query);
			if (rs != null) {
				if (rs.next()) {
					count = rs.getString("state_code");
					System.out.println("VID Count--->> " + count); 
				}
			}
		} catch (Exception e) {
			e.printStackTrace(new PrintWriter(stack));
			System.out.println("errpr is -->> " + stack.toString());
			System.out.println("error in fetching pincode state -->> " + stack.toString());
		} finally {
			try {
				if (conn != null)
					conn.close();
				if (statement != null)
					statement.close();
			} catch (Exception e2) {
				System.out.println("error in connection close-->> " + e2.getMessage()); 

			}
		}
		return count;

	}
	
	public String getEmployeepincode(int subs_id) {
		StringWriter stack = new StringWriter();
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String query = "";
		String count = null;

		try {
			query = "select a.office_pincode from tbl_subs_employee_history a where a.tbl_subscriber_subs_id="+ subs_id+"  AND a.is_active='Y' ORDER BY a.tbl_emp_id DESC LIMIT 1";
			conn =ConnectionUtil.getConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(query);
			if (rs != null) {
				if (rs.next()) {
					count = rs.getString("office_pincode");
					System.out.println("VID Count--->> " + count); 
				}
			}
		} catch (Exception e) {
			e.printStackTrace(new PrintWriter(stack));
			System.out.println("errpr is -->> " + stack.toString());
			System.out.println("error in fetching pincode state -->> " + stack.toString()); 
		} finally {
			try {
				if (conn != null)
					conn.close();
				if (statement != null)
					statement.close();
			} catch (Exception e2) {
				System.out.println("error in connection close-->> " + e2.getMessage()); 

			}
		}
		return count;

	}
}
