/**
 * 
 */
package com.es.client.business;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.equifax.services.eport.ws.schemas._1.RequestBodyType;
import com.equifax.services.eport.ws.schemas._1.StateCodeOptions;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BankAccountDetailsModel;
import com.es.client.model.BureauSubscriberDetailsModel;
import com.es.equifax.EquifaxRequest; 

/**
 * @author majidkhan
 *
 */
public class ConstructRequestBody {

	public RequestBodyType constructRetailRequest(List<BureauSubscriberDetailsModel> responseSubscriberDetail, String panId) {
		
		EquifaxRequest equifaxRequest = null;
		String dob = "1993-04-04";
		PincodeMappingDao pincodeState = null;
		String stateCode = null;
		int subs_id = 0;
		
		
		RequestBodyType requestBody = new RequestBodyType();
		BureauSubscriberDetailsModel bureauDetails=responseSubscriberDetail.get(0);
		
		/*for (BureauSubscriberDetails bureauDetails : responseSubscriberDetail) {*/
		equifaxRequest = new EquifaxRequest();
		String address1 = "", address2 = "", concatAddress = "";
		subs_id = bureauDetails.getSubscriberId();
		BankAccountDetailsModel bankAccountDetailsModel = new BureauDbDao().getBankName(subs_id);
		
		dob = bureauDetails.getDateOfBirth();
		equifaxRequest.setCity(bureauDetails.getCity());
		equifaxRequest.setGender(bureauDetails.getGender());
		
		if (bureauDetails.getAddress() != null && !bureauDetails.getAddress().isEmpty()) {
			address1 = bureauDetails.getAddress();
		}
		if (bureauDetails.getAddress2() != null && !bureauDetails.getAddress2().isEmpty()) {
			address2 = bureauDetails.getAddress2();
		}
		concatAddress = address1 + " , " + address2;
		
		if (concatAddress != null) {
			equifaxRequest.setAddress(concatAddress);
		} else {
			equifaxRequest.setAddress("");
		}
		
		if (bankAccountDetailsModel != null) {
			String lastname = "", first = "",firstname="";
			String regex = "^[a-zA-Z\\s.]+$";
			
			 firstname = bankAccountDetailsModel.getFirst_name().trim();
			 Pattern pattern = Pattern.compile(regex);
			 Matcher matcher = pattern.matcher(firstname);
				System.out.println(matcher.matches());
			 if (!matcher.matches()) {
				 equifaxRequest.setFirstName(bureauDetails.getFirstName());
					equifaxRequest.setLastName(bureauDetails.getLastName());
			}
			 
			 else if(bankAccountDetailsModel.getLast_name().trim().equals("")) {
			
				List<String> linked = new LinkedList<>();
				Stream.of(firstname.trim()).map(w -> w.split("\\s+")).flatMap(Arrays::stream)
						.collect(Collectors.toCollection(() -> linked));
				first = linked.stream().limit(linked.size() - 1).map(e -> e.concat(" "))
						.collect(Collectors.joining());
				//System.out.println("first name after java 8 :: "+first);
				if (first.trim().equals("")) {
					equifaxRequest.setFirstName(linked.get(0).trim());
					//System.out.println("first name is java 8 first name is empty :: "+equifaxRequest.getFirstName());
				} else {
					equifaxRequest.setFirstName(first.trim());
					//System.out.println("first name is java 8 first name is not empty :: "+equifaxRequest.getFirstName());
				}
				equifaxRequest.setLastName(linked.get(linked.size() - 1));
				//System.out.println("last name from java 8 :: "+equifaxRequest.getLastName());
			} else {
				equifaxRequest.setFirstName(firstname);
				equifaxRequest.setLastName(bankAccountDetailsModel.getLast_name());
				//System.out.println("bank last name :: "+equifaxRequest.getLastName());
			}
		} else {
			equifaxRequest.setFirstName(bureauDetails.getFirstName());
		//	System.out.println("if bank first name is not available :: "+equifaxRequest.getFirstName());
			equifaxRequest.setLastName(bureauDetails.getLastName());
		//	System.out.println("if bank last name is not available :: "+equifaxRequest.getLastName());
		}
		if (bureauDetails.getMobileNumber() != null) {
			equifaxRequest.setMobileNumber(bureauDetails.getMobileNumber());
		} else {
			equifaxRequest.setMobileNumber("1111111111");
		}
		if (bureauDetails.getState() != null) {
			equifaxRequest.setState(bureauDetails.getState());
		} else {
			equifaxRequest.setState("");
		}
		if (bureauDetails.getPincode() != null) {
			equifaxRequest.setPincode(bureauDetails.getPincode() + "");
		} else {
			equifaxRequest.setPincode("");
		}

	/*}*/
		
		String State = equifaxRequest.getState().toLowerCase();
		String inqPurpose = equifaxRequest.getInquiryPurpose();
		String firstName = equifaxRequest.getFirstName().toUpperCase();
		System.out.println("final first name  :: "+firstName);
		String lastName = equifaxRequest.getLastName().toUpperCase();
		System.out.println("final last name  :: "+lastName);
		String address = equifaxRequest.getAddress().toUpperCase();
		String pincode = equifaxRequest.getPincode();
		String pan = panId;
		String mob = equifaxRequest.getMobileNumber();

		System.out.println("Request Body" + " inqPurpose-->>" + inqPurpose + " ,firstName-->> " + firstName
				+ " ,lastName-->> " + lastName + " ,address-->> " + address + " ,pin-->> " + pincode
				+ " ,pan-->> " + pan + " ,mob-->> " + mob + " ,State-->> " + State);

		requestBody.setPANId(pan);
		requestBody.setMobilePhone(mob);
		
		if (firstName != null) {
			requestBody.setFirstName(firstName);
		} else {
			requestBody.setFirstName("FIRSTNAME");
		}
		if (lastName != null) {
			requestBody.setLastName(lastName);
		} else {
			requestBody.setLastName("LASTNAME");
		}
		if (inqPurpose != null) {
			requestBody.setInquiryPurpose(inqPurpose);
		} else {
			requestBody.setInquiryPurpose("02");
		}
		if (address != null) {
			requestBody.setAddrLine1(address);
		} else {
			requestBody.setAddrLine1("ADDRESS");
		}
		if (pincode != null) {
			requestBody.setPostal(pincode);
		} else {
			requestBody.setPostal("123456");
		}

		/*if (State != null && !State.equals("")) {*/
			pincodeState = new PincodeMappingDao();
			stateCode = pincodeState.getPincodeState(requestBody.getPostal());
			if (stateCode != null) {
				requestBody.setState(StateCodeOptions.valueOf(stateCode));
			} else {
				String emplyeeOfficePincode = pincodeState.getEmployeepincode(subs_id);
				stateCode = pincodeState.getPincodeState(emplyeeOfficePincode);
				requestBody.setPostal(emplyeeOfficePincode);
				requestBody.setState(StateCodeOptions.valueOf(stateCode));
			}
		/*}*/

		// Date Of Birth
		try {
			requestBody.setDOB(getXMLGregorianCalendarDate(dob));
			System.out.println("dobXml : " + requestBody.getDOB()); 
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return requestBody;

	}

	private XMLGregorianCalendar getXMLGregorianCalendarDate(String dateOfBirth) throws DatatypeConfigurationException {
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date dob;
		XMLGregorianCalendar dobXml = null;
		try {
			System.out.println("date of birth in getXMLGregorianCalendarDate method-->> " + dateOfBirth); 
			// TODO: Update your value
			dob = date.parse(dateOfBirth);
			GregorianCalendar gregory = new GregorianCalendar();
			gregory.setTime(dob);
			dobXml = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
		} catch (Exception e1) {
			e1.printStackTrace();

		}
		return dobXml;
	}
}
