/**
 * 
 */
package com.es.client.business;

import com.equifax.services.eport.ws.schemas._1.ReportFormatOptions;
import com.equifax.services.eport.ws.schemas._1.RequestHeaderType;
import com.es.equifax.EquifaxRequest;


public class ConstructRequestHeader {
	public RequestHeaderType populateUATRetailRequestHeaderType() {
		EquifaxRequest equifaxRequest = new EquifaxRequest();
		RequestHeaderType requestHeader = new RequestHeaderType();

		requestHeader.setCustomerId(equifaxRequest.getEquifaxCustId());
		requestHeader.setMemberNumber(equifaxRequest.getEquifaxMemberNumber());
		requestHeader.setPassword(equifaxRequest.getEquifaxPassword());
		requestHeader.setProductCode(equifaxRequest.getProductCode());
		requestHeader.setProductVersion(equifaxRequest.getProductVersion());
		requestHeader.setReportFormat(ReportFormatOptions.XML);
		requestHeader.setSecurityCode(equifaxRequest.getSecurityCode());
		requestHeader.setUserId(equifaxRequest.getEquifaxUserId());
		System.out.println("custId--> " + equifaxRequest.getEquifaxCustId() + "memberNo-->> "
				+ equifaxRequest.getEquifaxMemberNumber() + "pwd-->> " + equifaxRequest.getEquifaxPassword()
				+ "productCode-->> " + equifaxRequest.getProductCode() + "prodVersion-->> "
				+ equifaxRequest.getProductVersion() + "secCode-->> " + equifaxRequest.getSecurityCode() + "userId-->> "
				+ equifaxRequest.getEquifaxUserId());

		return requestHeader;
	}
}
