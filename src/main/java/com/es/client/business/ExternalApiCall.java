package com.es.client.business;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import com.es.client.model.FoirResponseModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author majidkhan
 *
 */
public class ExternalApiCall {
	private Gson gsonObj = null;

	public FoirResponseModel FoirApi(JSONObject obj, String api) {
		FoirResponseModel foirRes = new FoirResponseModel();
		HttpURLConnection con = null;
		DataOutputStream wr = null;
		try {
			URL url = new URL(api.trim());
			con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("charset", "utf-8");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestMethod("POST");
			wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(obj.toString());
			if (con.getResponseCode() == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				con.disconnect();
				this.gsonObj = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
				foirRes = (FoirResponseModel) this.gsonObj.fromJson(response.toString(), FoirResponseModel.class);

			} else {
				foirRes.setStatus_code(201);
				foirRes.setStatus_desc("Failure"); 
				System.out.println("Response code is other than 200");
			}
		} catch (Exception e) {
			foirRes.setStatus_code(201);
			foirRes.setStatus_desc("Failure");
			e.printStackTrace();
		} finally {
			try {
				if (wr != null) {
					wr.flush();
					wr.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return foirRes;
	}

	public String esApi(JSONObject obj, String api) {
		HttpURLConnection con = null;
		DataOutputStream wr = null;
		StringBuffer response=null;
		try {
			URL url = new URL(api.trim());
			con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("charset", "utf-8");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestMethod("POST");
			wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(obj.toString());
			if (con.getResponseCode() == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				 response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				con.disconnect();
			} else {
				System.out.println("Response code is other than 200 in es api call"); 
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (wr != null) {
					wr.flush();
					wr.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return response.toString();
	}

}
