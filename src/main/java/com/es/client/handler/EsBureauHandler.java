
package com.es.client.handler;

import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauReportResponseData;
import com.es.client.model.BureauSubscriberDetailsModel;
import com.es.client.model.BureauVo;
import com.es.client.model.RequestHandlerModel;
import com.es.equifax.EquifaxUtil;
import com.es.scores.update.AllScoresUpdate;
import com.google.gson.Gson;

public class EsBureauHandler implements RequestHandler<Object, String>{
	
	
	private final static Gson gson = new Gson();
	private final static Integer badRequest = 400;
	private final static Integer ALREADY_REPORTED = 208;
	private final static String prod_url = System.getenv("equifax_prod_url");

	enum RequestId {
		cirDetails, scoresUpdate
	}

	
	public BureauVo bureauHandler(Map<String, String> input, Context context) {
		String requestType = input.get("requestId");
		System.out.println("Reuqest ID::" + requestType);

		RequestId requestId = RequestId.valueOf(requestType);
		String payLoad = input.get("payLoad");
		System.out.println("PayLoad :" + payLoad);

		BureauVo bureauVo = new BureauVo();
		BureauDbDao bureauDbDao = null;
		
		RequestHandlerModel requestHandlerModel = gson.fromJson(payLoad, RequestHandlerModel.class);

		switch (requestId) {
		case cirDetails:
			
			System.out.println(requestId);
			bureauDbDao = new BureauDbDao();
			Integer subscriberId = requestHandlerModel.getSubscriberId();
			List<BureauSubscriberDetailsModel> responseSubscriberDetails = bureauDbDao.getSubsId(subscriberId);
																												
			if (responseSubscriberDetails.size() > 0) {
				System.out.println("Customer details Exist :: " + subscriberId);
				String panId = bureauDbDao.getPANId(subscriberId);
				System.out.println("Pancard filled by customer-->> " + panId);
				String bureauPanId = "";

				if (panId == null || panId.equals("")) {
					bureauVo = createresponse(badRequest, "PAN ID Does not Exists", false);
					System.out.println("PAN ID Does not Exists");
					System.out.println("Bureau Response :: " + gson.toJson(bureauVo));
					return bureauVo;
				} else {
					BureauReportResponseData data = bureauDbDao.checkBureauPanCard(panId, subscriberId);
					System.out.println("Bureau pan id before calling cir method-->> " + bureauPanId);
					System.out.println("Subs Id: " + subscriberId + " PAN NUMBER: " + panId
							+ "  BUreau pancared id --> " + bureauPanId);
					int bureauType = 1;
					String reqType = "CIR";
					if (data != null) {
						bureauVo = createresponse(ALREADY_REPORTED, "Bureau Score Already Exists with this PAN id",
								true);
						//bureauVo = new AllScoresUpdate().allScoresUpdate(bureauVo,subscriberId,requestHandlerModel.getSalary());
						bureauVo.setStatus_code("200");
						bureauVo.setSubs_id(subscriberId);
						bureauVo.setBureauId(requestHandlerModel.getBureauId());
						bureauVo.setCir(data.getScoreValue());
						bureauVo.setScoreElement(data.getScoringElementCode());
						System.out.println("Duplicate Pancard Number Requested For Bureau with Same subscriber id ");
						return bureauVo;
					} else {
						BureauDbDao dbDao = new BureauDbDao();
						int value = dbDao.checkHitpendingStatus(panId);
						if (value > 0) {
							bureauVo = createresponse(200, "Customer request in Pending", false);
							System.out.println("customer request is in pending-->> ");
							System.out.println("Bureau Response :: " + gson.toJson(bureauVo));
							return bureauVo;
						} else {
							EquifaxUtil equifaxUtil = new EquifaxUtil();
							bureauVo = equifaxUtil.invokeEquifaxWSService(prod_url, responseSubscriberDetails,
									bureauType, reqType, panId, requestHandlerModel.getBureauId(),
									requestHandlerModel.getSalary()); // Salary and Bureo id should be
																					// changed
							// = createresponse(HttpStatus.OK, "URL hitted", true);
							System.out.println("Bureau Response :: " + gson.toJson(bureauVo));
							System.out.println("-------------------------");
							return bureauVo;
						}
					}
				}
			}

			break;
		case scoresUpdate:
			
			System.out.println("scoresUpdate handler in ES Controller: ");
			System.out.println("requestId---->"+requestId);
			
			bureauVo = new AllScoresUpdate().allScoresUpdate(bureauVo, requestHandlerModel.getSubscriberId(),
					requestHandlerModel.getSalary());

			break;

		default:
			System.out.println("Unknown Request");
			break;

		} 
		
		return bureauVo;
		
	}

	private BureauVo createresponse(Integer status, String message, boolean flag) {
		BureauVo bureauVo = new BureauVo();
		try {
			bureauVo.setStatus(status);
			bureauVo.setMessage(message);
			bureauVo.setIs_success(flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bureauVo;
	}

	@Override
	public String handleRequest(Object input, Context context) {
		// TODO Auto-generated method stub
		return null;
	}

}