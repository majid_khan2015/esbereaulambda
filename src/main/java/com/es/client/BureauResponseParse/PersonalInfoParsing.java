/**
 * 
 */
package com.es.client.BureauResponseParse;

import com.equifax.services.eport.ws.schemas._1.PersonalInfoType;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauPersonalInformationModel;

/**
 * @author majidkhan
 *
 */
public class PersonalInfoParsing {

	public void personalInfoParsing(PersonalInfoType personalInfo, Integer bureauId) {
		BureauPersonalInformationModel bureauPersonalInformation = new BureauPersonalInformationModel();

		if (personalInfo.getName().getFirstName() != null) {
			bureauPersonalInformation.setFirstName(personalInfo.getName().getFirstName().toString());
		} else {
			bureauPersonalInformation.setFirstName("");
		}
		if (personalInfo.getName().getMiddleName() != null) {
			bureauPersonalInformation.setMiddleName(personalInfo.getName().getMiddleName().toString());
		} else {
			bureauPersonalInformation.setMiddleName("");
		}
		if (personalInfo.getName().getLastName() != null) {
			bureauPersonalInformation.setLastName(personalInfo.getName().getLastName().toString());
		} else {
			bureauPersonalInformation.setLastName("");
		}
		if (personalInfo.getDateOfBirth() != null) {
			bureauPersonalInformation.setDateOfBirth(personalInfo.getDateOfBirth().toString());
		} else {
			bureauPersonalInformation.setDateOfBirth("");
		}
		if (personalInfo.getGender() != null) {
			bureauPersonalInformation.setGender(personalInfo.getGender().toString());
		} else {
			bureauPersonalInformation.setGender("");
		}
		try {
			bureauPersonalInformation.setAge(personalInfo.getAge().getAge());
		} catch (Exception e) {
			e.printStackTrace();
		}

		bureauPersonalInformation.setIncome(personalInfo.getTotalIncome());
		bureauPersonalInformation.setOccupationType(personalInfo.getOccupation());
		/**
		 * data insert in mysql bureau_personal_info table
		 */
		try {
			BureauDbDao bureaudao = new BureauDbDao();
			bureaudao.insertDataToBureauPersonalInfo(bureauPersonalInformation, bureauId);

		} catch (Exception e) {
			System.out.println("Insert customer bureau personal info Exception--->");
			e.printStackTrace();

		}
	}
}
