package com.es.client.BureauResponseParse;

import com.equifax.services.eport.ws.schemas._1.InquiryResponseType;
import com.equifax.services.eport.ws.schemas._1.ScoringElementType;
import com.es.client.model.BureauReportResponseData;

public class BureauReportParser {

	public BureauReportResponseData bureauReportParser(InquiryResponseType response) {
		BureauReportResponseData bureauData = null;
		try {
			bureauData = new BureauReportResponseData();

			bureauData.setCustomerCode(response.getInquiryResponseHeader().getCustomerCode());
			bureauData.setClientId(response.getInquiryResponseHeader().getClientID());
			bureauData.setCustomerRefField("EarlySalary");
			bureauData.setReportOrderNo(response.getInquiryResponseHeader().getReportOrderNO());
			bureauData.setProductCode(response.getInquiryResponseHeader().getProductCode());
			bureauData.setSuccessCode(response.getInquiryResponseHeader().getSuccessCode());
			bureauData.setDate(response.getInquiryResponseHeader().getDate());
			bureauData.setTime(response.getInquiryResponseHeader().getTime());
			bureauData.setHitCode(response.getInquiryResponseHeader().getHitCode());
			bureauData.setInquiryPurpose(response.getInquiryRequestInfo().getInquiryPurpose());
			bureauData.setAddressLine(response.getInquiryRequestInfo().getAddrLine1());
			bureauData.setState(response.getInquiryRequestInfo().getState().value());

			bureauData.setPostalCode(response.getInquiryRequestInfo().getPostal());
			bureauData.setPhoneNo(response.getInquiryRequestInfo().getMobilePhone());
			bureauData.setPhoneType("M");
			bureauData.setPanId(response.getInquiryRequestInfo().getPANId());

			bureauData.setScoreName(response.getReportData().getScore().getName());
			bureauData.setScoreValue(response.getReportData().getScore().getValue());
			bureauData.setScoreDesc(response.getReportData().getScore().getDescription());
			bureauData.setScoreReasonCode(response.getReportData().getScore().getReasonCode());
			if (!response.getReportData().getScoringElements().getScoringElement().isEmpty()) {
				for (ScoringElementType scoringElementType : response.getReportData().getScoringElements()
						.getScoringElement()) {
					bureauData.setScoringElementCode(scoringElementType.getCode());
					bureauData.setScoringElementDesc(scoringElementType.getDescription());
					bureauData.setScoringElementType(scoringElementType.getType());
					bureauData.setScoringElementSeq(scoringElementType.getSeq());
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return bureauData;
	}
}
