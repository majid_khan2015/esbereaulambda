/**
 * 
 */
package com.es.client.BureauResponseParse;

import com.equifax.services.eport.ws.schemas._1.CreditReportSummaryType;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauAccountSummaryModel; 

/**
 * @author majidkhan
 *
 */
public class AccountSummaryParser {
	public void accountSummaryParser(Integer bureauId, CreditReportSummaryType accSumary) {
		BureauAccountSummaryModel bureauAccountSummary = new BureauAccountSummaryModel();

		bureauAccountSummary.setMostSevereStatusWithin24Months(accSumary.getMostSevereStatusWithIn24Months());
		bureauAccountSummary.setOldestAccount(accSumary.getOldestAccount());
		bureauAccountSummary.setRecentAccount(accSumary.getRecentAccount());
		bureauAccountSummary.setNoOfAccounts(accSumary.getNoOfAccounts());
		bureauAccountSummary.setNoOfActiveAccounts(accSumary.getNoOfActiveAccounts());
		bureauAccountSummary.setNoOfPastDueAccounts(accSumary.getNoOfPastDueAccounts());
		bureauAccountSummary.setNoOfWriteOffs(accSumary.getNoOfWriteOffs());
		bureauAccountSummary.setNoOfZeroBalAccounts(accSumary.getNoOfZeroBalanceAccounts());
		if (accSumary.getAverageOpenBalance() != null) {
			bureauAccountSummary.setAvgOpenBal(accSumary.getAverageOpenBalance().toString());
		} else {
			bureauAccountSummary.setAvgOpenBal("");
		}
		if (accSumary.getSingleHighestBalance() != null) {
			bureauAccountSummary.setSingleHighestBal(accSumary.getSingleHighestBalance().toString());
		} else {
			bureauAccountSummary.setSingleHighestBal("");
		}
		if (accSumary.getSingleHighestCredit() != null) {
			bureauAccountSummary.setSingleHighestCredit(accSumary.getSingleHighestCredit().toString());
		} else {
			bureauAccountSummary.setSingleHighestCredit("");
		}
		if (accSumary.getSingleHighestSanctionAmount() != null) {
			bureauAccountSummary.setSingleHighestSanctionAmount(accSumary.getSingleHighestSanctionAmount().toString());
		} else {
			bureauAccountSummary.setSingleHighestSanctionAmount("");
		}
		if (accSumary.getTotalBalanceAmount() != null) {
			bureauAccountSummary.setTotalBalAmount(accSumary.getTotalBalanceAmount().toString());
		} else {
			bureauAccountSummary.setTotalBalAmount("");
		}
		if (accSumary.getTotalCreditLimit() != null) {
			bureauAccountSummary.setTotalCreditLimit(accSumary.getTotalCreditLimit().toString());
		} else {
			bureauAccountSummary.setTotalCreditLimit(" ");
		}
		if (accSumary.getTotalHighCredit() != null) {
			bureauAccountSummary.setTotalHighCredit(accSumary.getTotalHighCredit().toString());
		} else {
			bureauAccountSummary.setTotalHighCredit("");
		}
		if (accSumary.getTotalMonthlyPaymentAmount() != null) {
			bureauAccountSummary.setTotalMonthlyPaymentAmount(accSumary.getTotalMonthlyPaymentAmount().toString());
		} else {
			bureauAccountSummary.setTotalMonthlyPaymentAmount("");
		}
		if (accSumary.getTotalPastDue() != null) {
			bureauAccountSummary.setTotalPastDue(accSumary.getTotalPastDue().toString());
		} else {
			bureauAccountSummary.setTotalPastDue("");
		}
		if (accSumary.getTotalSanctionAmount() != null) {
			bureauAccountSummary.setTotalSanctionAmount(accSumary.getTotalSanctionAmount().toString());
		} else {
			bureauAccountSummary.setTotalSanctionAmount("");
		}
		if (bureauAccountSummary != null) {
			/**
			 * data insert in mysql bureau_account_summary table
			 */
			try {
				BureauDbDao bureaudao = new BureauDbDao();

				bureaudao.insertDataToBureauAccountSummary(bureauAccountSummary, bureauId);

				// insertDataToBureauAccountSummary(bureauAccountSummary);
			} catch (Exception e) {
				System.out.println("customer bureau account summary info insert Exception---->");
				e.printStackTrace();
			}

		}
	}
}
