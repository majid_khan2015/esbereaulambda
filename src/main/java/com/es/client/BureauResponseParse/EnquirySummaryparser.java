/**
 * 
 */
package com.es.client.BureauResponseParse;

import com.equifax.services.eport.ws.schemas._1.EnquirySummaryType;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauEnquirySummaryModel; 

/**
 * @author majidkhan
 *
 */
public class EnquirySummaryparser {

	public void enquirySummaryparser(EnquirySummaryType enqSum, Integer bureauId) {
		BureauEnquirySummaryModel bureauEnquirySummary = new BureauEnquirySummaryModel();

		bureauEnquirySummary.setPast12Months(enqSum.getPast12Months());
		bureauEnquirySummary.setPast24Months(enqSum.getPast24Months());
		bureauEnquirySummary.setPast30Days(enqSum.getPast30Days());
		bureauEnquirySummary.setPurpose(enqSum.getPurpose());
		if (enqSum.getRecent() != null) {
			bureauEnquirySummary.setRecent(enqSum.getRecent().toString());
		} else {
			bureauEnquirySummary.setRecent("");
		}
		bureauEnquirySummary.setTotal(enqSum.getTotal());
		if (bureauEnquirySummary != null) {
			try {
				// data insert in mysql bureau_enquiry_summary table
				new BureauDbDao().insertDataToBureauEnquirySummary(bureauEnquirySummary, bureauId);
			} catch (Exception e) {
				System.out.println("customer bureau enquiry summary info insert Exception---->");
				e.printStackTrace();
			}
		}
	}
}
