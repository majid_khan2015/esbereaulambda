/**
 * 
 */
package com.es.client.BureauResponseParse;

import java.util.ArrayList;
import java.util.List;

import com.equifax.services.eport.ws.schemas._1.IDType;
import com.equifax.services.eport.ws.schemas._1.IdentificationType;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauIdentityInformationModel;

/**
 * @author majidkhan
 *
 */
public class IdentityInfoParsing {

	public void identityInfoParsing(Integer bureauId, IdentificationType identificationType) {
		List<BureauIdentityInformationModel> bureauIdentityList = new ArrayList<>();
		BureauIdentityInformationModel bureauIdentityInformation = null;

		try {
			int sizeOfPanId = identificationType.getPANId().size();
			int sizeOfNationalId = identificationType.getNationalIDCard().size();
			int sizeOfDriverLicense = identificationType.getDriverLicence().size();
			int sizeOfIdCard = identificationType.getIDCard().size();
			int sizeOfPassportId = identificationType.getPassportID().size();
			int sizeOfPhotoCreditCard = identificationType.getPhotoCreditCard().size();
			int sizeOfRationCard = identificationType.getRationCard().size();
			int sizeOfVoterId = identificationType.getVoterID().size();
			if (sizeOfPanId > 0) {
				for (IDType idType : identificationType.getPANId()) {
					bureauIdentityInformation = new BureauIdentityInformationModel();
					bureauIdentityInformation.setUniqueIdNo(idType.getIdNumber());
					if (idType.getReportedDate() != null) {
						bureauIdentityInformation.setUniqueIdReportedDate(idType.getReportedDate().toString());
					} else {
						bureauIdentityInformation.setUniqueIdReportedDate("");
					}
					bureauIdentityInformation.setId_type("PAN CARD");
					bureauIdentityInformation.setUniqueIdSeq(idType.getSeq());
					bureauIdentityList.add(bureauIdentityInformation);

				}
			}
			if (sizeOfNationalId > 0) {
				for (IDType idType : identificationType.getNationalIDCard()) {
					bureauIdentityInformation = new BureauIdentityInformationModel();
					bureauIdentityInformation.setUniqueIdNo(idType.getIdNumber());
					if (idType.getReportedDate() != null) {
						bureauIdentityInformation.setUniqueIdReportedDate(idType.getReportedDate().toString());
					} else {
						bureauIdentityInformation.setUniqueIdReportedDate("");
					}
					bureauIdentityInformation.setId_type("NATIONAL ID");
					bureauIdentityInformation.setUniqueIdSeq(idType.getSeq());
					bureauIdentityList.add(bureauIdentityInformation);

				}
			}

			if (sizeOfDriverLicense > 0) {
				for (IDType idType : identificationType.getDriverLicence()) {
					bureauIdentityInformation = new BureauIdentityInformationModel();
					bureauIdentityInformation.setUniqueIdNo(idType.getIdNumber());
					if (idType.getReportedDate() != null) {
						bureauIdentityInformation.setUniqueIdReportedDate(idType.getReportedDate().toString());
					} else {
						bureauIdentityInformation.setUniqueIdReportedDate("");
					}
					bureauIdentityInformation.setId_type("DRIVER LICENSE");
					bureauIdentityInformation.setUniqueIdSeq(idType.getSeq());
					bureauIdentityList.add(bureauIdentityInformation);

				}
			}
			if (sizeOfIdCard > 0) {
				for (IDType idType : identificationType.getIDCard()) {
					bureauIdentityInformation = new BureauIdentityInformationModel();
					bureauIdentityInformation.setUniqueIdNo(idType.getIdNumber());
					if (idType.getReportedDate() != null) {
						bureauIdentityInformation.setUniqueIdReportedDate(idType.getReportedDate().toString());
					} else {
						bureauIdentityInformation.setUniqueIdReportedDate("");
					}
					bureauIdentityInformation.setId_type("ID CARD");
					bureauIdentityInformation.setUniqueIdSeq(idType.getSeq());
					bureauIdentityList.add(bureauIdentityInformation);

				}
			}
			if (sizeOfPassportId > 0) {
				for (IDType idType : identificationType.getPassportID()) {
					bureauIdentityInformation = new BureauIdentityInformationModel();
					bureauIdentityInformation.setUniqueIdNo(idType.getIdNumber());
					if (idType.getReportedDate() != null) {
						bureauIdentityInformation.setUniqueIdReportedDate(idType.getReportedDate().toString());
					} else {
						bureauIdentityInformation.setUniqueIdReportedDate("");
					}
					bureauIdentityInformation.setId_type("PASSPORT");
					bureauIdentityInformation.setUniqueIdSeq(idType.getSeq());
					bureauIdentityList.add(bureauIdentityInformation);
				}
			}
			if (sizeOfPhotoCreditCard > 0) {
				for (IDType idType : identificationType.getPhotoCreditCard()) {
					bureauIdentityInformation = new BureauIdentityInformationModel();
					bureauIdentityInformation.setUniqueIdNo(idType.getIdNumber());
					if (idType.getReportedDate() != null) {
						bureauIdentityInformation.setUniqueIdReportedDate(idType.getReportedDate().toString());
					} else {
						bureauIdentityInformation.setUniqueIdReportedDate("");
					}
					bureauIdentityInformation.setId_type("PHOTO CREDIT CARD");
					bureauIdentityInformation.setUniqueIdSeq(idType.getSeq());
					bureauIdentityList.add(bureauIdentityInformation);
				}
			}
			if (sizeOfRationCard > 0) {
				for (IDType idType : identificationType.getRationCard()) {
					bureauIdentityInformation = new BureauIdentityInformationModel();
					bureauIdentityInformation.setUniqueIdNo(idType.getIdNumber());
					if (idType.getReportedDate() != null) {
						bureauIdentityInformation.setUniqueIdReportedDate(idType.getReportedDate().toString());
					} else {
						bureauIdentityInformation.setUniqueIdReportedDate("");
					}
					bureauIdentityInformation.setId_type("RATION CARD");
					bureauIdentityInformation.setUniqueIdSeq(idType.getSeq());
					bureauIdentityList.add(bureauIdentityInformation);
				}
			}
			if (sizeOfVoterId > 0) {
				for (IDType idType : identificationType.getVoterID()) {
					bureauIdentityInformation = new BureauIdentityInformationModel();
					bureauIdentityInformation.setUniqueIdNo(idType.getIdNumber());
					if (idType.getReportedDate() != null) {
						bureauIdentityInformation.setUniqueIdReportedDate(idType.getReportedDate().toString());
					} else {
						bureauIdentityInformation.setUniqueIdReportedDate("");
					}
					bureauIdentityInformation.setId_type("VOTER ID");
					bureauIdentityInformation.setUniqueIdSeq(idType.getSeq());
					bureauIdentityList.add(bureauIdentityInformation);
				}
			}
			/**
			 * Insertion in bureau identification info table
			 */
			if (bureauIdentityList.size() > 0) {
				BureauDbDao bureaudao = new BureauDbDao();
				bureaudao.insertDataToBureauIdentityInfo(bureauIdentityList, bureauId);
			}
		} catch (Exception e) {
			System.out.println("Error in customer identity information--->");
			e.printStackTrace();
		}

	}
}
