/**
 * 
 */
package com.es.client.BureauResponseParse;

import java.util.List;

import com.equifax.services.eport.ws.schemas._1.ScoringElementType;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauScoringElementsModel;

/**
 * @author majidkhan
 *
 */
public class ScoreElementParser {
	
	public void scoreElementParser(Integer BureauId, List<ScoringElementType> score) {
		BureauScoringElementsModel scoreElement = null;
		for (ScoringElementType scoringElementType : score) {
			scoreElement = new BureauScoringElementsModel();
			scoreElement.setScore_code(scoringElementType.getCode());
			scoreElement.setScore_desc(scoringElementType.getDescription());
			scoreElement.setScore_sequence(scoringElementType.getSeq());
			scoreElement.setScore_type(scoringElementType.getType());
			/**
			 * data insert in mysql bureau_score_elements table
			 */
			try {
				new BureauDbDao().insertDataToBureauScoreElements(scoreElement, BureauId);
			} catch (Exception e) {
				System.out.println("customer bureau score info insert Exception--->");
				e.printStackTrace();

			}

		}
	}
}
