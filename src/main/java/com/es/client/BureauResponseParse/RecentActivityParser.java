/**
 * 
 */
package com.es.client.BureauResponseParse;

import com.equifax.services.eport.ws.schemas._1.RecentActivitiesType;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauRecentActivityInfoModel; 

/**
 * @author majidkhan
 *
 */
public class RecentActivityParser {

	public void recentActivityParser(Integer bureauId, RecentActivitiesType recentActivity) {
		BureauRecentActivityInfoModel recentInfo = new BureauRecentActivityInfoModel();
		try {
			recentInfo.setAccountsDeliquent(recentActivity.getAccountsDeliquent());
			recentInfo.setAccountsOpened(recentActivity.getAccountsOpened());
			recentInfo.setAccountsUpdated(recentActivity.getAccountsUpdated());
			recentInfo.setTotalEnquiries(recentActivity.getTotalInquiries());
			if (recentInfo != null) {
				// data insert in mysql bureau_recent_activity_info table
				new BureauDbDao().insertDataToBureauRecentActivityInfo(recentInfo, bureauId);
			}
		} catch (Exception e) {
			System.out.println("customer bureau recent activity info insert Exception--->");
			e.printStackTrace();

		}

	}
}
