/**
 * 
 */
package com.es.client.BureauResponseParse;

import java.util.ArrayList;
import java.util.List;

import com.equifax.services.eport.ws.schemas._1.AddressType;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauAddressInformationModel;

/**
 * @author majidkhan
 *
 */
public class AddressParsing {

	public void addressparsing(List<AddressType> address, Integer bureauId) {

		List<BureauAddressInformationModel> addressList = new ArrayList<>();
		BureauAddressInformationModel bureauAddressInformation = null;
		try {
			for (AddressType addressType : address) {
				bureauAddressInformation = new BureauAddressInformationModel();
				if (addressType.getAddress() != null && addressType.getAddress() != "") {
					bureauAddressInformation.setAddress(addressType.getAddress());
				} else {
					bureauAddressInformation.setState("");
				}
				if (addressType.getState() != null && addressType.getState() != "") {
					bureauAddressInformation.setState(addressType.getState());
				} else {
					bureauAddressInformation.setState("");
				}
				if (addressType.getPostal() != null && !addressType.getPostal().equals("")) {
					bureauAddressInformation.setPostalCode(addressType.getPostal());
				} else {
					bureauAddressInformation.setPostalCode(0);
				}
				if (addressType.getType() != null && addressType.getType() != "") {
					bureauAddressInformation.setType(addressType.getType());
				} else {
					bureauAddressInformation.setType("");
				}
				addressList.add(bureauAddressInformation);
			}
			// data insert in mysql bureau_address_info table
			if (addressList.size() > 0) {
				new BureauDbDao().insertDataToBureauAddressInformation(addressList, bureauId);
			}
		} catch (Exception e) {
			System.out.println("customer bureau address info process Exception--->");
			e.printStackTrace();

		}

	}
}
