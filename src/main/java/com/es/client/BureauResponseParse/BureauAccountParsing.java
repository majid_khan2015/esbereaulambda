/**
 * 
 */
package com.es.client.BureauResponseParse;

import java.util.ArrayList;
import java.util.List;

import com.equifax.services.eport.ws.schemas._1.AccountDetailsType;
import com.equifax.services.eport.ws.schemas._1.AccountType;
import com.equifax.services.eport.ws.schemas._1.MonthlyDetailType;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauAccountDetailsModel;
import com.es.client.model.BureauHistory48MonthsModel; 

/**
 * @author majidkhan
 *
 */
public class BureauAccountParsing {
	public void bureauAccountsParsing(AccountDetailsType accouts, Integer bureauId) {
		for (AccountType accountType : accouts.getAccount()) {
			BureauAccountDetailsModel accountDetails = new BureauAccountDetailsModel();

			accountDetails.setAccountNo(accountType.getAccountNumber());
			if (accountType.getReportedDate() != null) {
				accountDetails.setAccountReportedDate(accountType.getReportedDate().toString());
			} else {
				accountDetails.setAccountReportedDate("");
			}
			accountDetails.setAccountSeqNo(accountType.getSeq());
			accountDetails.setAccountType(accountType.getAccountType());
			accountDetails.setBalance(accountType.getBalance());
			if (accountType.getDateOpened() != null) {
				accountDetails.setDateOpened(accountType.getDateOpened().toString());
			} else {
				accountDetails.setDateOpened("");
			}
			if (accountType.getDateReported() != null) {
				accountDetails.setDateReported(accountType.getDateReported().toString());
			} else {
				accountDetails.setDateReported("");
			}
			accountDetails.setDisputeCode(accountType.getDisputeCode());
			accountDetails.setInstitutionName(accountType.getInstitution()); // found
			accountDetails.setIntrestRate(accountType.getInterestRate());
			accountDetails.setLastPayment(accountType.getLastPayment());
			if (accountType.getLastPaymentDate() != null) {
				accountDetails.setLastPaymentDate(accountType.getLastPaymentDate().toString());
			} else {
				accountDetails.setLastPaymentDate("");
			}
			accountDetails.setOpen(accountType.getOpen());
			accountDetails.setOwnershipType(accountType.getOwnershipType());
			accountDetails.setPastDueAmount(accountType.getPastDueAmount());
			accountDetails.setReason(accountType.getReason());
			accountDetails.setRepaymentTenure(accountType.getRepaymentTenure());
			accountDetails.setSanctionAmount(accountType.getSanctionAmount());
			accountDetails.setTermFrequency(accountType.getTermFrequency());
			accountDetails.setCollateralType(accountType.getCollateralType());
			accountDetails.setCollateralValue(accountType.getCollateralValue());
			accountDetails.setInstallmentAmount(accountType.getInstallmentAmount());
			if (accountType.getDateClosed() != null) {
				accountDetails.setDateClosed(accountType.getDateClosed().toString());
			} else {
				accountDetails.setDateClosed("");
			}
			if (accountType.getAccountStatus() != null) {
				accountDetails.setAccountStatus(accountType.getAccountStatus());
			} else {
				accountDetails.setAccountStatus("No Status");
			}
			accountDetails.setAssetClassification(accountType.getAssetClassification());
			accountDetails.setSuitFiledStatus(accountType.getSuitFiledStatus());

			// new fields added
			accountDetails.setHighCredit(accountType.getHighCredit());
			accountDetails.setCreditLimit(accountType.getCreditLimit());
			/**
			 * data insert in mysql bureau_accounts_details and bureau_history_48_months
			 * table
			 */
			try {

				BureauDbDao bureaudao = new BureauDbDao();
				/**
				 * data insert in bureau_accounts_details
				 */
				int account_id = bureaudao.insertDataToBureauAccountDetails(accountDetails, bureauId);
				if (account_id > 0) {
					int accountDetailsId = account_id;
					List<BureauHistory48MonthsModel> bureau48Month = new ArrayList<BureauHistory48MonthsModel>();
					BureauHistory48MonthsModel bureauHistory48Months = null;
					for (MonthlyDetailType monthlyDetailType : accountType.getHistory48Months().getMonth()) {
						bureauHistory48Months = new BureauHistory48MonthsModel();
						bureauHistory48Months
								.setAssetClassifiationStatus(monthlyDetailType.getAssetClassificationStatus());
						bureauHistory48Months.setMonthKey(monthlyDetailType.getKey());
						bureauHistory48Months.setPaymentStatus(monthlyDetailType.getPaymentStatus());
						bureauHistory48Months.setSuitFiledStatus(monthlyDetailType.getSuitFiledStatus());
						bureau48Month.add(bureauHistory48Months);

					}
					try {
						/**
						 * data insert in bureau_history_48_months
						 */
						bureaudao.insertDataToBureauHistory48Months(bureau48Month, accountDetailsId);
					} catch (Exception e) {
						System.out.println("insertion into bureau_history_48_months error---->");
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				System.out.println("customer bureau account details info error--->");
				e.printStackTrace();
			}
		}
	}
}
