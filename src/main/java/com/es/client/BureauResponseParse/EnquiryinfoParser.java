/**
 * 
 */
package com.es.client.BureauResponseParse;

import java.util.List;

import com.equifax.services.eport.ws.schemas._1.EnquiryType;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauEnquiresInfoModel;

/**
 * @author majidkhan
 *
 */
public class EnquiryinfoParser {
	
	public void enquiryinfoParser(Integer bureauId, List<EnquiryType> enquiry) {
		BureauEnquiresInfoModel bureauEnquiresInfo = null;
		for (EnquiryType enquiryType : enquiry) {
			bureauEnquiresInfo = new BureauEnquiresInfoModel();
			bureauEnquiresInfo.setAmount(enquiryType.getAmount());
			if (enquiryType.getDate() != null) {
				bureauEnquiresInfo.setDate(enquiryType.getDate().toString());
			} else {
				bureauEnquiresInfo.setDate("");
			}
			bureauEnquiresInfo.setInstitution(enquiryType.getInstitution());
			bureauEnquiresInfo.setRequestPurpose(enquiryType.getRequestPurpose());
			bureauEnquiresInfo.setSequenceNumber(enquiryType.getSeq());
			bureauEnquiresInfo.setTime(enquiryType.getTime());
			/**
			 * data insert in mysql bureau_enquiries_info table
			 */
			try {
				new BureauDbDao().insertDataToBureauEnquiriesInfo(bureauEnquiresInfo, bureauId);
				// insertDataToBureauEnquiriesInfo(bureauEnquiresInfo);
			} catch (Exception e) {
				System.out.println("customer bureau enquiry  info insert Exception--->");
				e.printStackTrace();

			}
		}
	}
}
