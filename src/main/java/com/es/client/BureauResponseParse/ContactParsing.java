/**
 * 
 */
package com.es.client.BureauResponseParse;

import java.util.ArrayList;
import java.util.List;

import com.equifax.services.eport.ws.schemas._1.PhoneType;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauContactInformationModel;

/**
 * @author majidkhan
 *
 */
public class ContactParsing {

	public void contactparsing(List<PhoneType> phone, Integer bureauId) {
		List<BureauContactInformationModel> contactList = new ArrayList<>();
		BureauContactInformationModel bureauContactInformation = null;
		for (PhoneType phoneType : phone) {
			bureauContactInformation = new BureauContactInformationModel();
			if (phoneType.getAreaCode() != null && phoneType.getAreaCode() != "") {
				bureauContactInformation.setAreaCode(phoneType.getAreaCode());
			} else {
				bureauContactInformation.setAreaCode("");
			}
			if (phoneType.getCountryCode() != null && phoneType.getCountryCode() != "") {
				bureauContactInformation.setCountryCode(phoneType.getCountryCode());
			} else {
				bureauContactInformation.setCountryCode("");
			}
			if (phoneType.getNumber() != null && phoneType.getNumber() != "") {
				bureauContactInformation.setContactNo(phoneType.getNumber());
			} else {
				bureauContactInformation.setContactNo("");
			}
			if (phoneType.getPhoneNumberExtension() != null && phoneType.getPhoneNumberExtension() != "") {
				bureauContactInformation.setContactNoExtension(phoneType.getPhoneNumberExtension());

			} else {
				bureauContactInformation.setContactNoExtension("");
			}
			if (phoneType.getReportedDate() != null) {
				bureauContactInformation.setContactReportedDate(phoneType.getReportedDate().toString());
			} else {
				bureauContactInformation.setContactReportedDate("");
			}
			if (phoneType.getSeq() != null) {
				bureauContactInformation.setContactSeq(phoneType.getSeq());

			} else {
				bureauContactInformation.setContactSeq(Integer.parseInt(""));
			}
			if (phoneType.getTypeCode() != null) {
				bureauContactInformation.setContactTypeCode(phoneType.getTypeCode().toString());
			} else {
				bureauContactInformation.setContactTypeCode("");
			}
			contactList.add(bureauContactInformation);
		}

		try {

			// data insert in mysql bureau_contact_info table
			new BureauDbDao().insertDataToBureauContactInformation(contactList, bureauId);

		} catch (Exception e) {
			System.out.println("customer bureau contact  info insert Exception--->");
			e.printStackTrace();
		}
	}
}
