/**
 * 
 */
package com.es.client.BureauResponseParse;

import com.equifax.services.eport.ws.schemas._1.OtherKeyIndType;
import com.es.client.dao.BureauDbDao;
import com.es.client.model.BureauOtherKeyIdentificationModel;

/**
 * @author majidkhan
 *
 */
public class OtherKeyIdentifyParser {

	public void otherKeyIdentifyParser(Integer bureauId, OtherKeyIndType otherKeyIndType) {
		BureauOtherKeyIdentificationModel otherKey = new BureauOtherKeyIdentificationModel();
		try {
			otherKey.setAgeOfOldestTrade(otherKeyIndType.getAgeOfOldestTrade());
			otherKey.setAllLinesEverWritten(otherKeyIndType.getAllLinesEVERWritten());
			otherKey.setAllLinesEverWrittenIn6Months(otherKeyIndType.getAllLinesEVERWrittenIn6Months());
			otherKey.setAllLinesEverWrittenIn9Months(otherKeyIndType.getAllLinesEVERWrittenIn9Months());
			otherKey.setNoOfOpenTrades(otherKeyIndType.getNumberOfOpenTrades());
			if (otherKey != null) {
				// data insert in mysql bureau_other_key_identification table
				new BureauDbDao().insertDataToBureauOtherKeyIdentification(otherKey, bureauId);
			}
		} catch (Exception e) {
			System.out.println("customer bureau other key identification info insert Exception---->");
			e.printStackTrace();

		}

	}
}
